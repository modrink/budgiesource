/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Osvaldas Čiupkevičius
package com.pagiringi.budgeteer;

import android.widget.TextView;

public class Money
{
    private double amount;
    private Currency currency;

    public Money(double amount, Currency currency)
    {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount()
    {
        return this.amount;
    }

    public Currency getCurrency()
    {
        return this.currency;
    }

    /**
     * Function is called to set the money view.
     * @param view
     */
    public void setMoneyView(TextView view, boolean changeColor)
    {

        if (changeColor && getAmount() != 0)
        {
            int red = view.getResources().getColor(R.color.red);
            int green = view.getResources().getColor(R.color.green);
            view.setTextColor(getAmount() < 0 ? red : green);
        }

        view.setText(formatText());
    }

    /**
     * Function is called to format a money text.
     * @return
     */
    private String formatText()
    {
        return String.format("%.2f %s", getAmount(), getCurrency());
    }

    public enum Currency
    {
        EUR("Euro", "€"), USD("Dollar", "$");

        private final String name;
        private final String symbol;

        Currency(String name, String symbol)
        {
            this.name = name;
            this.symbol = symbol;
        }
    }
}
