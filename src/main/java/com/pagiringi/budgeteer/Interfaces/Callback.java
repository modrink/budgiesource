/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Interfaces;

public interface Callback<T>
{
    void Return(T obj);
}
