/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class DialogContact extends DialogFragment
{
    private User currentUser = new User();
    private boolean error = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        currentUser.uid = User.getCurrentUid();
        currentUser.load(null);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_contact, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going sum the dialog layout
        builder.setView(view).setPositiveButton(getString(R.string.apply), (dialog, id) ->
        {
        }).setNeutralButton(getString(R.string.clear), (dialog, id) ->
        {
        }).setNegativeButton(getString(R.string.cancel), (dialog, id) ->
        {
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        final AlertDialog d = (AlertDialog) getDialog();
        if (d != null)
        {
            TextInputLayout emailLayout = d.findViewById(R.id.contact_email_layout);
            TextInputEditText emailText = d.findViewById(R.id.contact_email);

            d.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(v ->
            {
                error = false;
                Pattern pattern = Patterns.EMAIL_ADDRESS;
                String email = emailText.getText().toString();
                if (email.isEmpty())
                {
                    emailLayout.setError(getString(R.string.field_required));
                    error = true;
                }
                else if (!pattern.matcher(email).matches())
                {
                    emailLayout.setError(getString(R.string.invalid_email));
                    error = true;
                }
                if (!error)
                {
                    User u = new User();
                    u.email = email;
                    u.load(res ->
                    {
                        if (res == null)
                        {
                            emailLayout.setError(getString(R.string.user_not_found));
                            error = true;
                            return;
                        }
                        else if (res.uid.equals(User.getCurrentUid()))
                        {
                            emailLayout.setError(getString(R.string.self_friend));
                            error = true;
                            return;
                        }

                        Contact user = new Contact();
                        user.userID = currentUser.uid;
                        user.contactID = res.uid;
                        user.load(res2 ->
                        {
                            if (res2.uid != null && !res2.uid.isEmpty())
                            {
                                emailLayout.setError(getString(R.string.contact_exists));
                                error = true;
                                return;
                            }

                            if (!error)
                            {
                                res2.customEmail = res.email;
                                res2.customPhone = res.phone;
                                res2.contactName = res.name;
                                res2.contactPhoto = res.photo;
                                res2.save(res3 ->
                                {
                                    Contact c2 = new Contact();
                                    c2.userID = res.uid;
                                    c2.contactID = currentUser.uid;
                                    c2.customEmail = currentUser.email;
                                    c2.customPhone = currentUser.phone;
                                    c2.contactName = currentUser.name;
                                    c2.contactPhoto = currentUser.photo;
                                    c2.save(null);
                                });
                                d.dismiss();
                            }
                        });
                    });
                }
            });
            d.getButton(Dialog.BUTTON_NEUTRAL).setOnClickListener(v -> emailText.setText(""));
            d.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(v -> d.dismiss());
        }
    }
}
