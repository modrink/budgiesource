/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class DialogAccount extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_account, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going sum the dialog layout
        builder.setView(view).setPositiveButton(getString(R.string.apply), (dialog, id) ->
        {
        }).setNeutralButton(getString(R.string.clear), (dialog, id) ->
        {
        }).setNegativeButton(getString(R.string.cancel), (dialog, id) ->
        {
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        final AlertDialog d = (AlertDialog) getDialog();
        if (d != null)
        {
            TextInputLayout nameLayout = d.findViewById(R.id.account_name_layout);
            TextInputEditText nameText = d.findViewById(R.id.account_name);

            d.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(v ->
            {
                String name = nameText.getText().toString();
                if (name.isEmpty())
                {
                    nameLayout.setError(getString(R.string.field_required));
                }
                else
                {
                    Account a = new Account();
                    a.name = name;
                    a.userID = User.getCurrentUid();
                    a.save(res -> d.dismiss());
                }
            });
            d.getButton(Dialog.BUTTON_NEUTRAL).setOnClickListener(v -> nameText.setText(""));
            d.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(v -> d.dismiss());
        }
    }
}
