/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.Query;
import com.pagiringi.budgeteer.Firestore.Payment;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.Firestore.Tag;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class DialogFilter extends DialogFragment
{
    private View view;
    private DialogFilterListener listener;
    private String[] sortFilters;
    private String[] orderFilters;
    private Set<String> tagFilters;
    private Spinner sortBy;
    private Spinner sortOrder;
    private Spinner tags;
    private EditText sumMin;
    private EditText sumMax;
    private EditText dateFrom;
    private EditText dateTo;

    private int currentSort = 0;
    private int currentOrder = 0;
    private int currentTag = 0;
    private String currentSumMin;
    private String currentSumMax;
    private String currentDateFrom;
    private String currentDateTo;

    public DialogFilter()
    {
        this.listener = null;
    }

    public void setListener(DialogFilterListener listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_filter, null);

        // Fill dropdown with data
        sortFilters = new String[]{getString(R.string.date_bill), getString(R.string.sum_plain),
                getString(R.string.title_plain)};
        orderFilters = new String[]{getString(R.string.ascending), getString(R.string.descending)};
        tagFilters = new HashSet<>();
        tagFilters.add(getString(R.string.none));

        Tag t = new Tag();
        t.userID = User.getCurrentUid();
        t.loadTags(res ->
        {
            for (int i = 0; i < res.size(); i++)
            {
                tagFilters.add(res.get(i).name);
            }
            setTagFilters();
        });

        // Sort by
        sortBy = view.findViewById(R.id.sort_filters);
        ArrayAdapter<String> sortByData =
                new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_item, sortFilters);
        sortByData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBy.setAdapter(sortByData);

        // Order by
        sortOrder = view.findViewById(R.id.sort_order);
        ArrayAdapter<String> sortOrderData =
                new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_item, orderFilters);
        sortOrderData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortOrder.setAdapter(sortOrderData);

        // Sum
        sumMin = view.findViewById(R.id.sum_min);
        sumMax = view.findViewById(R.id.sum_max);

        // Date
        dateFrom = view.findViewById(R.id.date_from);
        dateTo = view.findViewById(R.id.date_to);

        // Tags


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going sum the dialog layout
        builder.setView(view).setPositiveButton(getString(R.string.apply), (dialog, id) ->
        {
        }).setNeutralButton(getString(R.string.clear), (dialog, id) ->
        {
        }).setNegativeButton(getString(R.string.cancel), (dialog, id) ->
        {
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    private void setTagFilters()
    {
        // Tags
        tags = view.findViewById(R.id.tag);
        ArrayAdapter tagData =
                new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_item, tagFilters.toArray());
        tagData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tags.setAdapter(tagData);
        tags.setSelection(currentTag);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        final AlertDialog d = (AlertDialog) getDialog();

        sortOrder.setSelection(currentOrder);
        sortBy.setSelection(currentSort);
        sumMin.setText(currentSumMin);
        sumMax.setText(currentSumMax);
        dateTo.setText(currentDateTo);
        dateFrom.setText(currentDateFrom);

        if (d != null)
        {
            d.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(v ->
            {
                Query q = Split.getCollection().whereEqualTo("userID", User.getCurrentUid());
                String field = "customDate";
                Query.Direction direction = Query.Direction.ASCENDING;
                currentSort = sortBy.getSelectedItemPosition();
                currentOrder = sortOrder.getSelectedItemPosition();
                currentTag = tags.getSelectedItemPosition();
                currentSumMin = sumMin.getText().toString();
                currentSumMax = sumMax.getText().toString();
                currentDateFrom = dateFrom.getText().toString();
                currentDateTo = dateTo.getText().toString();
                boolean dismiss = true;
                boolean dateWhere = false;
                String localePattern =
                        ((SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()))
                                .toPattern();
                SimpleDateFormat dateFormat = new SimpleDateFormat(localePattern, Locale.getDefault());

                String sortOrderText = sortOrder.getSelectedItem().toString().toLowerCase();
                if (sortOrderText.equals(getString(R.string.ascending).toLowerCase()))
                {
                    direction = Query.Direction.ASCENDING;
                }
                else if (sortOrderText.equals(getString(R.string.descending).toLowerCase()))
                {
                    direction = Query.Direction.DESCENDING;
                }

                String sortByText = sortBy.getSelectedItem().toString().toLowerCase();
                if (sortByText.equals(getString(R.string.date_bill).toLowerCase()))
                {
                    field = "customDate";
                }
                else if (sortByText.equals(getString(R.string.sum_plain).toLowerCase()))
                {
                    field = "sum";
                }
                else if (sortByText.equals(getString(R.string.title_plain).toLowerCase()))
                {
                    field = "customTitle";
                }

                String tagsText = tags.getSelectedItem().toString().toLowerCase();
                if (!tagsText.equals(getString(R.string.none).toLowerCase()))
                {
                    q = q.whereArrayContains("tags", tagsText);
                }

                if (!currentDateFrom.isEmpty())
                {
                    dateWhere = true;
                    Date currentDateFromDate = null;
                    try
                    {
                        currentDateFromDate = dateFormat.parse(currentDateFrom);
                    }
                    catch (ParseException e)
                    {
                        dismiss = false;
                        dateFrom.setError(getString(R.string.invalid_from_date) + localePattern);
                    }

                    if (currentDateFromDate != null)
                    {
                        q = q.whereGreaterThanOrEqualTo("customDate", new Timestamp(currentDateFromDate));
                    }
                }

                if (!currentDateTo.isEmpty())
                {
                    dateWhere = true;
                    Date currentDateToDate = null;
                    try
                    {
                        currentDateToDate = dateFormat.parse(currentDateTo);
                    }
                    catch (ParseException e)
                    {
                        dismiss = false;
                        dateTo.setError(getString(R.string.invalid_to_date) + localePattern);
                    }

                    if (currentDateToDate != null)
                    {
                        q = q.whereLessThanOrEqualTo("customDate", new Timestamp(currentDateToDate));
                    }
                }

                if (!field.equals("customDate") && (!currentDateFrom.isEmpty() || !currentDateTo.isEmpty()))
                {
                    q = q.orderBy("customDate", Query.Direction.ASCENDING);
                }

                if (!currentSumMin.isEmpty())
                {
                    if (dateWhere)
                    {
                        sumMin.setError(getString(R.string.sum_date_incompatible));
                        dismiss = false;
                    }
                    else
                    {
                        q = q.whereGreaterThanOrEqualTo("sum", Float.valueOf(currentSumMin));
                    }
                }

                if (!currentSumMax.isEmpty())
                {
                    if (dateWhere)
                    {
                        sumMax.setError(getString(R.string.sum_date_incompatible));
                        dismiss = false;
                    }
                    else
                    {
                        q = q.whereLessThanOrEqualTo("sum", Float.valueOf(currentSumMax));
                    }
                }

                if (!dateWhere && !field.equals("sum") && (!currentSumMin.isEmpty() || !currentSumMax.isEmpty()))
                {
                    q = q.orderBy("sum", Query.Direction.ASCENDING);
                }

                if (dismiss)
                {
                    q = q.orderBy(field, direction);
                    listener.onQueryReady(q);
                    d.dismiss();
                }
            });
            d.getButton(Dialog.BUTTON_NEUTRAL).setOnClickListener(v ->
            {
                sortBy.setSelection(0);
                sortOrder.setSelection(0);
                tags.setSelection(0);
                sumMin.setText(null);
                sumMax.setText(null);
                dateFrom.setText(null);
                dateTo.setText(null);
            });
            d.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(v -> d.dismiss());
        }
    }

    public interface DialogFilterListener
    {
        void onQueryReady(Query query);
    }
}
