/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer;

import android.app.Application;

import androidx.lifecycle.ProcessLifecycleOwner;

public class BudgieApp extends Application
{

    @Override
    public void onCreate()
    {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new LifecycleTracker());
    }
}
