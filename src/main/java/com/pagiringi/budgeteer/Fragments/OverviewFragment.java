/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity;
import com.pagiringi.budgeteer.Adapters.AccountAdapter;
import com.pagiringi.budgeteer.Adapters.DebtAdapter;
import com.pagiringi.budgeteer.Dialogs.DialogAccount;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.Money;
import com.pagiringi.budgeteer.R;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OverviewFragment extends Fragment
{
    private float accountsTotal;
    private float debtsTotal;

    private RecyclerView.LayoutManager accountLM;
    private RecyclerView.LayoutManager debtLM;
    private RecyclerView accountRV;
    private RecyclerView debtRV;
    private AccountAdapter accountAdapter;
    private DebtAdapter debtAdapter;

    private TextView availableFunds;
    private TextView accountTotal;
    private TextView debtTotal;

    @Override
    public void onStart()
    {
        super.onStart();
        accountAdapter.startListening();
        debtAdapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        accountAdapter.stopListening();
        debtAdapter.stopListening();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        // Set text.
        availableFunds = view.findViewById(R.id.available_funds);
        accountTotal = view.findViewById(R.id.balance_total);
        debtTotal = view.findViewById(R.id.debt_total);

        // Implement speed dial
        SpeedDialView SDV = view.findViewById(R.id.speedDial);
        SDV.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_payment,
                R.drawable.ic_description_white_24dp).setLabel(getString(R.string.new_payment))
                .setTheme(R.style.AppTheme_FAB)
                .create());
        SDV.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_account,
                R.drawable.ic_balance_white_24dp).setLabel(getString(R.string.new_account))
                .setTheme(R.style.AppTheme_FAB)
                .create());
        SDV.setOnActionSelectedListener(actionItem ->
        {
            switch (actionItem.getId())
            {
                case R.id.fab_account:
                    DialogFragment dialog = new DialogAccount();
                    dialog.show(requireActivity().getSupportFragmentManager(), "DialogAccount");
                    return false;
                case R.id.fab_payment:
                    startActivity(new Intent(getActivity(), PaymentEditActivity.class));
                    return false;
                default:
                    return false;
            }
        });

        // Account list
        Query accountQuery = Account.getCollection().whereEqualTo("userID", User.getCurrentUid());
        FirestoreRecyclerOptions<Account> accountOptions =
                new FirestoreRecyclerOptions.Builder<Account>().setQuery(accountQuery, Account.class)
                        .build();
        accountRV = view.findViewById(R.id.account_list);
        accountRV.setHasFixedSize(true);
        accountLM = new LinearLayoutManager(view.getContext());
        accountRV.setLayoutManager(accountLM);
        accountAdapter = new AccountAdapter(accountOptions);
        accountAdapter.setListener(this::updateBalance);
        accountRV.setAdapter(accountAdapter);

        // Debt list
        Query debtQuery = Contact.getCollection().whereEqualTo("userID", User.getCurrentUid());
        FirestoreRecyclerOptions<Contact> debtOptions =
                new FirestoreRecyclerOptions.Builder<Contact>().setQuery(debtQuery, Contact.class)
                        .build();
        debtRV = view.findViewById(R.id.debt_list);
        debtRV.setHasFixedSize(true);
        debtLM = new LinearLayoutManager(view.getContext());
        debtRV.setLayoutManager(debtLM);
        debtAdapter = new DebtAdapter(debtOptions);
        debtAdapter.setListener(this::updateDebts);
        debtRV.setAdapter(debtAdapter);
        return view;
    }

    //Osvaldas Čiupkevičius
    private void updateBalance(float accountsTotal)
    {
        this.accountsTotal = accountsTotal;
        new Money(accountsTotal, Money.Currency.EUR).setMoneyView(accountTotal, true);
        new Money(accountsTotal + debtsTotal, Money.Currency.EUR).setMoneyView(availableFunds,
                true);
    }

    //Osvaldas Čiupkevičius
    private void updateDebts(float debtsTotal)
    {
        this.debtsTotal = debtsTotal;
        new Money(debtsTotal, Money.Currency.EUR).setMoneyView(debtTotal, true);
        new Money(accountsTotal + debtsTotal, Money.Currency.EUR).setMoneyView(availableFunds,
                true);
    }
}
