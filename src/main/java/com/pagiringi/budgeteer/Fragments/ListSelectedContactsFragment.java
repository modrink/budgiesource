/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedContactDTO;
import com.pagiringi.budgeteer.Adapters.SelectedContactListAdapter;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link SelectedContactsListener}
 * interface.
 */
public class ListSelectedContactsFragment extends Fragment
{
    private RecyclerView RV;
    private SelectedContactListAdapter adapter;
    private RecyclerView.LayoutManager LM;
    private SelectedContactsListener listener;
    private ArrayList<SelectedContactDTO> contacts = new ArrayList<>();

    public ArrayList<SelectedContactDTO> getSplitList()
    {
        return adapter.getItems();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.list_contact_selected, container, false);

        RV = view.findViewById(R.id.selectedContactsList);
        RV.setHasFixedSize(true);

        LM = new LinearLayoutManager(view.getContext());
        RV.setLayoutManager(LM);

        adapter = new SelectedContactListAdapter(contacts, listener);
        RV.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof SelectedContactsListener)
        {
            listener = (SelectedContactsListener) context;
        }
        else
        {
            throw new RuntimeException(
                    context.toString() + " must implement SelectedContactsListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    public void setContacts(ArrayList<SelectedContactDTO> selectedUsers)
    {
        contacts = selectedUsers;
        adapter = new SelectedContactListAdapter(contacts, listener);
        if (RV != null)
        {
            RV.setAdapter(adapter);
        }
    }

    public interface SelectedContactsListener
    {
        void onRemove(SelectedContactDTO user);
    }
}
