/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedAccountDTO;
import com.pagiringi.budgeteer.Adapters.SelectedAccountListAdapter;
import com.pagiringi.budgeteer.Adapters.SelectedContactListAdapter;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link SelectedAccountsListener}
 * interface.
 */
public class ListSelectedAccountsFragment extends Fragment
{
    private RecyclerView RV;
    private SelectedAccountListAdapter adapter;
    private RecyclerView.LayoutManager LM;
    private SelectedAccountsListener listener;
    private ArrayList<SelectedAccountDTO> contacts = new ArrayList<>();

    public ArrayList<SelectedAccountDTO> getSplitList()
    {
        return adapter.getItems();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.list_contact_selected, container, false);

        RV = view.findViewById(R.id.selectedContactsList);
        RV.setHasFixedSize(true);

        LM = new LinearLayoutManager(view.getContext());
        RV.setLayoutManager(LM);

        adapter = new SelectedAccountListAdapter(contacts, listener, getContext());
        RV.setAdapter(adapter);

        TextView selectedText = view.findViewById(R.id.selected_text);
        selectedText.setText(getString(R.string.selected_accounts));

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof SelectedAccountsListener)
        {
            listener = (SelectedAccountsListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString() + " must implement SelectedAccountsListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    public void setContacts(ArrayList<SelectedAccountDTO> selectedUsers)
    {
        contacts = selectedUsers;
        adapter = new SelectedAccountListAdapter(contacts, listener, getContext());
        if (RV != null)
        {
            RV.setAdapter(adapter);
        }
    }

    public interface SelectedAccountsListener
    {
        void onRemove(SelectedAccountDTO user);
    }
}
