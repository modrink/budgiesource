/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.pagiringi.budgeteer.Adapters.ContactAdapter;
import com.pagiringi.budgeteer.Dialogs.DialogContact;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ContactsFragment extends Fragment
{
    private View view;

    private RecyclerView RV;
    private RecyclerView.LayoutManager LM;
    private ContactAdapter adapter;

    @Override
    public void onStart()
    {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        FloatingActionButton addContact = view.findViewById(R.id.add_contact);
        addContact.setOnClickListener(view1 ->
        {
            DialogContact dialog = new DialogContact();
            dialog.show(requireActivity().getSupportFragmentManager(), "DialogContact");
        });

        // FirebaseUI
        Query query = Contact.getCollection().whereEqualTo("userID", User.getCurrentUid());
        FirestoreRecyclerOptions<Contact> options =
                new FirestoreRecyclerOptions.Builder<Contact>().setQuery(query, Contact.class)
                        .build();
        RV = view.findViewById(R.id.contact_list);
        RV.setHasFixedSize(true);
        LM = new LinearLayoutManager(view.getContext());
        RV.setLayoutManager(LM);
        adapter = new ContactAdapter(options);
        RV.setAdapter(adapter);
        return view;
    }
}
