/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.pagiringi.budgeteer.Adapters.SplitAdapter;
import com.pagiringi.budgeteer.Dialogs.DialogFilter;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.Money;
import com.pagiringi.budgeteer.R;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PaymentsFragment extends Fragment
{
    private RecyclerView RV;
    private RecyclerView.LayoutManager LM;
    private SplitAdapter adapter;

    private TextView incomeText;
    private TextView expenditureText;
    private TextView totalText;

    private DialogFilter dialog;

    @Override
    public void onStart()
    {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_payments, container, false);

        incomeText = view.findViewById(R.id.bill_income);
        expenditureText = view.findViewById(R.id.bill_expenditure);
        totalText = view.findViewById(R.id.bill_total);

        // FirebaseUI
        Query query = Split.getCollection().whereEqualTo("userID", User.getCurrentUid());
        FirestoreRecyclerOptions<Split> options =
                new FirestoreRecyclerOptions.Builder<Split>().setQuery(query, Split.class).build();
        RV = view.findViewById(R.id.bill_list);
        RV.setHasFixedSize(true);
        LM = new LinearLayoutManager(view.getContext());
        RV.setLayoutManager(LM);
        adapter = new SplitAdapter(options);
        adapter.setListener(this::updateTotals);
        RV.setAdapter(adapter);

        // DialogFilter
        dialog = new DialogFilter();
        dialog.setListener(this::updateAdapter);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_filter, menu);

        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_filter:
                // Create an instance of the dialog fragment and show it
                dialog.show(requireActivity().getSupportFragmentManager(), "DialogFilter");
                return true;
            default:
                break;
        }
        return false;
    }

    private void updateAdapter(Query query)
    {
        updateTotals(0,0);
        FirestoreRecyclerOptions<Split> options =
                new FirestoreRecyclerOptions.Builder<Split>().setQuery(query, Split.class).build();
        adapter.stopListening();
        adapter = new SplitAdapter(options);
        adapter.setListener(this::updateTotals);
        RV.setAdapter(adapter);
        adapter.startListening();
    }

    private void updateTotals(float income, float expense)
    {
        new Money(income, Money.Currency.EUR).setMoneyView(incomeText, true);
        new Money(expense, Money.Currency.EUR).setMoneyView(expenditureText, true);
        new Money(income + expense, Money.Currency.EUR).setMoneyView(totalText, true);
    }
}
