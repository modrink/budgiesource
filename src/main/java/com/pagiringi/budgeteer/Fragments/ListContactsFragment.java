/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pagiringi.budgeteer.Adapters.ContactListAdapter;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnSelectListener}
 * interface.
 */
public class ListContactsFragment extends Fragment
{
    private RecyclerView RV;
    private RecyclerView.Adapter A;
    private RecyclerView.LayoutManager LM;
    private OnSelectListener listener;
    private List<Contact> contacts = new ArrayList<>();
    private String filter = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.list_contact, container, false);

        RV = view.findViewById(R.id.contactList);
        RV.setHasFixedSize(true);

        LM = new LinearLayoutManager(view.getContext());
        RV.setLayoutManager(LM);

        A = new ContactListAdapter(contacts, listener);
        applyFilter();

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnSelectListener)
        {
            listener = (OnSelectListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString() + " must implement OnSelectListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    private void applyFilter()
    {
        if (A != null)
        {
            ((ContactListAdapter) A).getFilter().filter(filter);
        }
        if (RV != null)
        {
            RV.setAdapter(A);
        }
    }

    public void setFilter(String f)
    {
        filter = f;
        applyFilter();
    }

    public void setContacts(List<Contact> remainingContacts)
    {
        contacts = remainingContacts;
        A = new ContactListAdapter(contacts, listener);
        applyFilter();
    }

    public interface OnSelectListener
    {
        void onSelect(Contact contact);
    }
}
