/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.preference.*;

import com.github.omadahealth.lollipin.lib.managers.AppLock;
import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.pagiringi.budgeteer.Activities.CustomPinActivity;
import com.pagiringi.budgeteer.R;

import androidx.preference.PreferenceFragmentCompat;

public class SettingsFragment extends PreferenceFragmentCompat
{
    private LockManager<CustomPinActivity> lockManager = LockManager.getInstance();

    @Override
    public void onResume()
    {
        super.onResume();
        onLockState(lockManager.getAppLock().isPasscodeSet());
    }

    private void onLockState(boolean state)
    {
        Preference pinChange = findPreference("pin_change");
        Preference pinDisable = findPreference("pin_disable");
        Preference pinEnable = findPreference("pin_enable");

        if (state)
        {
            pinChange.setVisible(true);
            pinDisable.setVisible(true);
            pinEnable.setVisible(false);
        }
        else
        {
            pinChange.setVisible(false);
            pinDisable.setVisible(false);
            pinEnable.setVisible(true);
        }

        pinChange.setOnPreferenceClickListener(preference ->
        {
            Intent intent = new Intent(getActivity().getBaseContext(), CustomPinActivity.class);
            intent.putExtra(AppLock.EXTRA_TYPE, AppLock.CHANGE_PIN);
            startActivityForResult(intent, 12);
            return true;
        });
        pinDisable.setOnPreferenceClickListener(preference ->
        {
            Intent intent = new Intent(getActivity().getBaseContext(), CustomPinActivity.class);
            intent.putExtra(AppLock.EXTRA_TYPE, AppLock.DISABLE_PINLOCK);
            startActivityForResult(intent, 13);
            return true;
        });
        pinEnable.setOnPreferenceClickListener(preference ->
        {
            Intent intent = new Intent(getActivity().getBaseContext(), CustomPinActivity.class);
            intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK);
            startActivityForResult(intent, 11);
            return true;
        });
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
    {
        setPreferencesFromResource(R.xml.settings_security, rootKey);
    }
}
