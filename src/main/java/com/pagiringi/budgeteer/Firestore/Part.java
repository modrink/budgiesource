/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */

package com.pagiringi.budgeteer.Firestore;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.List;

//Modestas Rinkevičius
public class Part
{
    private static String tableName = "parts";

    @Exclude
    public String uid;
    public String splitID;
    public String accountID;
    public float sum;
    public String title;
    @ServerTimestamp
    public Timestamp date;
    @ServerTimestamp
    public Timestamp updated;

    public Part()
    {
    }

    private void set(Part p)
    {
        uid = p.uid;
        splitID = p.splitID;
        accountID = p.accountID;
        sum = p.sum;
        date = p.date;
        updated = p.updated;
    }

    public void load(Callback<Part> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Part p = res.toObject(Part.class);
                if (p != null)
                {
                    p.uid = res.getId();
                    set(p);
                }
                cb.Return(this);
            });
        }
        else if (accountID != null && splitID != null)
        {
            getCollection().whereEqualTo("accountID", accountID)
                    .whereEqualTo("splitID", splitID)
                    .get()
                    .addOnSuccessListener(res ->
                    {
                        Part s = res.toObjects(Part.class).get(0);
                        if (s != null)
                        {
                            s.uid = res.getDocuments().get(0).getId();
                            set(s);
                        }
                        cb.Return(this);
                    });
        }
    }

    public void delete(Callback<Part> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).delete().addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void save(Callback<Part> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();

                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void loadParts(Callback<List<Part>> cb)
    {
        getCollection().whereEqualTo("splitID", splitID).get().addOnSuccessListener(res ->
        {
            List<Part> list = res.toObjects(Part.class);
            for (int i = 0; i < res.size(); i++)
            {
                list.get(i).uid = res.getDocuments().get(i).getId();
            }
            cb.Return(list);
        });
    }

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }
    //endregion


}
