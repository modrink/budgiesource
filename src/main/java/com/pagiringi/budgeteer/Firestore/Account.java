/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.ArrayList;
import java.util.List;

public class Account implements Parcelable
{
    private static String tableName = "accounts";

    @Exclude
    public String uid;
    public String userID;
    public String name;
    public float income;
    public float expense;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public Account()
    {
    }

    protected Account(Parcel in)
    {
        uid = in.readString();
        userID = in.readString();
        name = in.readString();
        income = in.readFloat();
        expense = in.readFloat();
        created = in.readParcelable(Timestamp.class.getClassLoader());
        updated = in.readParcelable(Timestamp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(uid);
        dest.writeString(userID);
        dest.writeString(name);
        dest.writeFloat(income);
        dest.writeFloat(expense);
        dest.writeParcelable(created, flags);
        dest.writeParcelable(updated, flags);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<Account> CREATOR = new Creator<Account>()
    {
        @Override
        public Account createFromParcel(Parcel in)
        {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size)
        {
            return new Account[size];
        }
    };

    private void set(Account b)
    {
        uid = b.uid;
        userID = b.userID;
        name = b.name;
        income = b.income;
        expense = b.expense;
        created = b.created;
        updated = b.updated;
    }

    public void load(Callback<Account> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Account b = res.toObject(Account.class);
                if (b != null)
                {
                    b.uid = res.getId();
                    set(b);
                }
                cb.Return(this);
            });
        }
    }

    public void save(Callback<Account> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    @Override
    public String toString()
    {
        return name;
    }
    //endregion

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }

    //endregion
}
