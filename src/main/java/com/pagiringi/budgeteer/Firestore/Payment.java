/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

public class Payment implements Parcelable
{
    private static String tableName = "payments";

    @Exclude
    public String uid;
    public String userID;
    public String title;
    public Timestamp date;
    public float sum;
    public String description;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public Payment()
    {
    }

    //region Parcelable implementation
    protected Payment(Parcel in)
    {
        uid = in.readString();
        title = in.readString();
        date = in.readParcelable(Timestamp.class.getClassLoader());
        description = in.readString();
        userID = in.readString();
        sum = in.readFloat();
        created = in.readParcelable(Timestamp.class.getClassLoader());
        updated = in.readParcelable(Timestamp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(uid);
        dest.writeString(title);
        dest.writeParcelable(date, flags);
        dest.writeString(description);
        dest.writeString(userID);
        dest.writeFloat(sum);
        dest.writeParcelable(created, flags);
        dest.writeParcelable(updated, flags);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>()
    {
        @Override
        public Payment createFromParcel(Parcel in)
        {
            return new Payment(in);
        }

        @Override
        public Payment[] newArray(int size)
        {
            return new Payment[size];
        }
    };
    //endregion

    private void set(Payment b)
    {
        uid = b.uid;
        title = b.title;
        date = b.date;
        description = b.description;
        userID = b.userID;
        sum = b.sum;
        created = b.created;
        updated = b.updated;
    }

    public void load(Callback<Payment> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Payment p = res.toObject(Payment.class);
                if (p != null)
                {
                    p.uid = res.getId();
                    set(p);
                }
                cb.Return(this);
            });
        }
    }

    public void save(Callback<Payment> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();

                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void delete(Callback<Payment> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).delete().addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }
    //endregion
}
