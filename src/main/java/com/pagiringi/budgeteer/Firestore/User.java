/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.ArrayList;
import java.util.List;

public class User implements Comparable<User>, Parcelable
{
    private static String tableName = "users";

    @Exclude
    public String uid;
    public String mainAccountID;
    public String name;
    public String email;
    public String phone;
    public String photo;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public User()
    {
    }

    //region Parcelable implementation
    protected User(Parcel in)
    {
        uid = in.readString();
        email = in.readString();
        mainAccountID = in.readString();
        name = in.readString();
        phone = in.readString();
        photo = in.readString();
        created = in.readParcelable(Timestamp.class.getClassLoader());
        updated = in.readParcelable(Timestamp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(uid);
        dest.writeString(email);
        dest.writeString(mainAccountID);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(photo);
        dest.writeParcelable(created, flags);
        dest.writeParcelable(updated, flags);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>()
    {
        @Override
        public User createFromParcel(Parcel in)
        {
            return new User(in);
        }

        @Override
        public User[] newArray(int size)
        {
            return new User[size];
        }
    };
    //endregion

    private void set(User u)
    {
        uid = u.uid;
        email = u.email;
        mainAccountID = u.mainAccountID;
        name = u.name;
        phone = u.phone;
        photo = u.photo;
        created = u.created;
        updated = u.updated;
    }

    //Osvaldas Čiupkevičius
    public User loadFirebaseData()
    {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser == null)
        {
            return null;
        }

        uid = firebaseUser.getUid();
        name = firebaseUser.getDisplayName();
        email = firebaseUser.getEmail();
        photo = null;
        if (firebaseUser.getPhotoUrl() != null)
        {
            photo = firebaseUser.getPhotoUrl().toString();
        }
        phone = firebaseUser.getPhoneNumber();

        return this;
    }

    public void load(Callback<User> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                User u = res.toObject(User.class);
                if (u != null)
                {
                    u.uid = res.getId();
                    set(u);
                }
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else if (email != null)
        {
            getCollection().whereEqualTo("email", email).get().addOnSuccessListener(res ->
            {
                if (res.size() > 0)
                {
                    DocumentSnapshot doc = res.getDocuments().get(0);
                    User u = doc.toObject(User.class);
                    u.uid = doc.getId();
                    set(u);
                    cb.Return(this);
                }
                else
                {
                    cb.Return(null);
                }
            });
        }
        else
        {
            cb.Return(null);
        }
    }

    public static void getUserAccounts(Callback<List<Account>> cb)
    {
        Account.getCollection().whereEqualTo("userID", getCurrentUid()).get().addOnSuccessListener(res ->
        {
            List<Account> list = res.toObjects(Account.class);
            for (int i = 0; i < res.size(); i++)
            {
                list.get(i).uid = res.getDocuments().get(i).getId();
            }
            cb.Return(list);
        });
    }

    public static void getUserContacts(Callback<List<Contact>> cb)
    {
        Contact.getCollection().whereEqualTo("userID", getCurrentUid()).get().addOnSuccessListener(res ->
        {
            List<Contact> list = res.toObjects(Contact.class);
            for (int i = 0; i < res.size(); i++)
            {
                list.get(i).uid = res.getDocuments().get(i).getId();
            }
            cb.Return(list);
        });
    }

    public void save(Callback<User> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();

                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    //region Overrides
    @Override
    public int compareTo(User o)
    {
        return name.toLowerCase().compareTo(o.name.toLowerCase());
    }

    @Override
    public String toString()
    {
        return "User{" + "uid='" + uid + '\'' + ", email='" + email + '\'' + ", mainAccountID='" +
                mainAccountID + '\'' + ", name='" + name + '\'' + ", phone='" + phone + '\'' +
                ", photo='" + photo + '\'' + '}';
    }
    //endregion

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }

    //Tomas Kučinskas
    public static boolean isOnline()
    {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    //Osvaldas Čiupkevičius
    public static String getCurrentUid()
    {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser == null)
        {
            return null;
        }

        return firebaseUser.getUid();
    }
    //endregion
}
