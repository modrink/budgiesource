/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.List;

public class Contact implements Parcelable
{
    private static String tableName = "contacts";

    @Exclude
    public String uid;
    public String userID;
    public String contactID;
    public String contactName;
    public String contactPhoto;
    public String customEmail;
    public String customPhone;
    public String customDescription;
    public float in;
    public float out;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public Contact()
    {
    }

    //region Parcelable implementation
    protected Contact(Parcel in)
    {
        uid = in.readString();
        userID = in.readString();
        contactID = in.readString();
        contactName = in.readString();
        contactPhoto = in.readString();
        customEmail = in.readString();
        customPhone = in.readString();
        customDescription = in.readString();
        this.in = in.readFloat();
        out = in.readFloat();
        created = in.readParcelable(Timestamp.class.getClassLoader());
        updated = in.readParcelable(Timestamp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(uid);
        dest.writeString(userID);
        dest.writeString(contactID);
        dest.writeString(contactName);
        dest.writeString(contactPhoto);
        dest.writeString(customEmail);
        dest.writeString(customPhone);
        dest.writeString(customDescription);
        dest.writeFloat(in);
        dest.writeFloat(out);
        dest.writeParcelable(created, flags);
        dest.writeParcelable(updated, flags);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>()
    {
        @Override
        public Contact createFromParcel(Parcel in)
        {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size)
        {
            return new Contact[size];
        }
    };
    //endregion

    private void set(Contact c)
    {
        uid = c.uid;
        userID = c.userID;
        contactID = c.contactID;
        contactName = c.contactName;
        contactPhoto = c.contactPhoto;
        customEmail = c.customEmail;
        customPhone = c.customPhone;
        customDescription = c.customDescription;
        in = c.in;
        out = c.out;
        created = c.created;
        updated = c.updated;
    }

    public void load(Callback<Contact> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Contact c = res.toObject(Contact.class);
                if (c != null)
                {
                    c.uid = res.getId();
                    set(c);
                }
                cb.Return(this);
            });
        }
        else if (contactID != null && userID != null)
        {
            getCollection().whereEqualTo("userID", userID)
                    .whereEqualTo("contactID", contactID)
                    .get()
                    .addOnSuccessListener(res ->
                    {
                        if (res.getDocuments().size() > 0)
                        {
                            DocumentSnapshot doc = res.getDocuments().get(0);
                            Contact c = doc.toObject(Contact.class);
                            c.uid = doc.getId();
                            set(c);
                        }
                        cb.Return(this);
                    });
        }
        else
        {
            cb.Return(this);
        }
    }

    public void loadList(List<Integer> IDList)
    {

    }

    public void save(Callback<Contact> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();

                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    @Override
    public String toString()
    {
        return contactName + ' ' + customEmail;
    }

    public void delete(Callback<Contact> cb)
    {
        getCollection().document(uid).delete().addOnSuccessListener(res -> cb.Return(this));
    }


    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }
    //endregion
}