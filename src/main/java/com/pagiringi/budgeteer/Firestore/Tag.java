/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.ArrayList;
import java.util.List;

public class Tag
{
    private static String tableName = "tags";

    @Exclude
    public String uid;
    public String splitID;
    public String userID;
    public String name;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public Tag()
    {
    }

    private void set(Tag t)
    {
        uid = t.uid;
        splitID = t.splitID;
        name = t.name;
        userID = t.userID;
        created = t.created;
        updated = t.updated;
    }

    public void load(Callback<Tag> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Tag t = res.toObject(Tag.class);
                if (t != null)
                {
                    t.uid = res.getId();
                    set(t);
                }
                cb.Return(this);
            });
        }
        else if (name != null && splitID != null && userID != null)
        {
            getCollection().whereEqualTo("name", name)
                    .whereEqualTo("splitID", splitID)
                    .whereEqualTo("userID", userID)
                    .get()
                    .addOnSuccessListener(res ->
                    {
                        Tag t = res.toObjects(Tag.class).get(0);
                        if (t != null)
                        {
                            t.uid = res.getDocuments().get(0).getId();
                            set(t);
                        }
                        cb.Return(this);
                    });
        }
    }

    public void loadTags(Callback<List<Tag>> cb)
    {
        if (splitID != null && userID != null)
        {
            getCollection().whereEqualTo("splitID", splitID)
                    .whereEqualTo("userID", userID)
                    .get()
                    .addOnSuccessListener(res ->
                    {
                        List<Tag> list = res.toObjects(Tag.class);
                        for (int i = 0; i < res.size(); i++)
                        {
                            list.get(i).uid = res.getDocuments().get(i).getId();
                        }
                        cb.Return(list);
                    });
        }
        else if (userID != null)
        {
            getCollection().whereEqualTo("userID", userID).get().addOnSuccessListener(res ->
            {
                List<Tag> list = res.toObjects(Tag.class);
                for (int i = 0; i < res.size(); i++)
                {
                    list.get(i).uid = res.getDocuments().get(i).getId();
                }
                cb.Return(list);
            });
        }
    }

    public void delete(Callback<Tag> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).delete().addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void save(Callback<Tag> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }
    //endregion
}
