/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Firestore;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.pagiringi.budgeteer.Interfaces.Callback;

import java.util.List;

public class Split
{
    private static String tableName = "splits";

    @Exclude
    public String uid;
    public String userID;
    public String payerID;
    public String receiverID;
    public String paymentID;
    public List<String> tags;
    public float total;
    public float sum;
    public float share;
    public float repaid;
    public String customTitle;
    public Timestamp customDate;
    public String customDescription;
    @ServerTimestamp
    public Timestamp created;
    @ServerTimestamp
    public Timestamp updated;

    public Split()
    {
    }

    private void set(Split s)
    {
        uid = s.uid;
        userID = s.userID;
        payerID = s.payerID;
        receiverID = s.receiverID;
        paymentID = s.paymentID;
        total = s.total;
        sum = s.sum;
        share = s.share;
        repaid = s.repaid;
        customTitle = s.customTitle;
        customDate = s.customDate;
        customDescription = s.customDescription;
        created = s.created;
        updated = s.updated;
        tags = s.tags;
    }

    public void load(Callback<Split> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).get().addOnSuccessListener(res ->
            {
                Split s = res.toObject(Split.class);
                if (s != null)
                {
                    s.uid = res.getId();
                    set(s);
                }
                cb.Return(this);
            });
        }
        else if (paymentID != null && userID != null)
        {
            getCollection().whereEqualTo("paymentID", paymentID)
                    .whereEqualTo("userID", userID)
                    .get()
                    .addOnSuccessListener(res ->
                    {
                        Split s = res.toObjects(Split.class).get(0);
                        if (s != null)
                        {
                            s.uid = res.getDocuments().get(0).getId();
                            set(s);
                        }
                        cb.Return(this);
                    });
        }
    }

    public void save(Callback<Split> cb)
    {
        updated = null;
        if (uid == null)
        {
            getCollection().add(this).addOnSuccessListener(res ->
            {
                uid = res.getId();

                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
        else
        {
            getCollection().document(uid).set(this).addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void delete(Callback<Split> cb)
    {
        if (uid != null)
        {
            getCollection().document(uid).delete().addOnSuccessListener(res ->
            {
                if (cb != null)
                {
                    cb.Return(this);
                }
            });
        }
    }

    public void loadSplits(Callback<List<Split>> cb)
    {
        getCollection().whereEqualTo("paymentID", paymentID).get().addOnSuccessListener(res ->
        {
            List<Split> list = res.toObjects(Split.class);
            for (int i = 0; i < res.size(); i++)
            {
                list.get(i).uid = res.getDocuments().get(i).getId();
            }
            cb.Return(list);
        });
    }

    //region Static methods
    public static CollectionReference getCollection()
    {
        return FirebaseFirestore.getInstance().collection(tableName);
    }
    //endregion
}
