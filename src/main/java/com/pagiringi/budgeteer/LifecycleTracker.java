/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer;

import android.util.Log;

import com.github.omadahealth.lollipin.lib.managers.LockManager;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class LifecycleTracker implements LifecycleObserver
{
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onLPause()
    {
        LockManager.getInstance().getAppLock().setTimeout(10000);
    }
}
