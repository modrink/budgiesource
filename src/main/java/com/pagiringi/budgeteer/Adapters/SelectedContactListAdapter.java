/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedContactDTO;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.Fragments.ListSelectedContactsFragment;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class SelectedContactListAdapter
        extends RecyclerView.Adapter<SelectedContactListAdapter.ViewHolder>
{
    private final ArrayList<SelectedContactDTO> contacts;
    private final ListSelectedContactsFragment.SelectedContactsListener listener;

    public SelectedContactListAdapter(ArrayList<SelectedContactDTO> contacts,
            ListSelectedContactsFragment.SelectedContactsListener listener)
    {
        this.contacts = contacts;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_selected_contact,
                        parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int i)
    {
        SelectedContactDTO model = contacts.get(i);
        if (model.contact.contactPhoto != null)
        {
            Glide.with(holder.avatar.getContext())
                    .load(model.contact.contactPhoto)
                    .into(holder.avatar);
        }
        else
        {
            Glide.with(holder.avatar.getContext())
                    .load(R.drawable.ic_account_circle_secondary_24dp)
                    .into(holder.avatar);
        }
        holder.name.setText(model.contact.contactName);
        holder.email.setText(model.contact.customEmail);
        holder.share.setText(model.share);
        holder.sum.setText(model.sum);
        holder.button.setOnClickListener(v ->
        {
            if (listener != null)
            {
                listener.onRemove(model);
            }
        });
        if (model.contact.contactID.equals(User.getCurrentUid()))
        {
            holder.button.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        return contacts.size();
    }

    public ArrayList<SelectedContactDTO> getItems()
    {
        return contacts;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final ImageView avatar;
        public final TextView name;
        public final TextView email;
        public final ImageButton button;
        public final EditText share;
        public final EditText sum;

        public ViewHolder(View view)
        {
            super(view);
            avatar = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            email = view.findViewById(R.id.custom);
            button = view.findViewById(R.id.remove);
            share = view.findViewById(R.id.share);
            sum = view.findViewById(R.id.sum);

            share.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {

                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    contacts.get(getAdapterPosition()).share = s.toString();
                }
            });

            sum.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    contacts.get(getAdapterPosition()).sum = s.toString();
                }
            });
        }
    }
}