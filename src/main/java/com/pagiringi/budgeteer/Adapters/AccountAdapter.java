/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.AccountActivity;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.R;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Locale;

public class AccountAdapter extends FirestoreRecyclerAdapter<Account,AccountAdapter.AccountHolder>
{
    private AccountAdapterListener listener;
    public float income;
    public float expense;

    public AccountAdapter(FirestoreRecyclerOptions<Account> options)
    {
        super(options);
        this.listener = null;
    }

    public void setListener(AccountAdapterListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(AccountHolder holder, int position, Account model)
    {
        String ID = getSnapshots().getSnapshot(position).getId();

        holder.name.setText(model.name);
        holder.amount.setText(String.format(Locale.getDefault(), "%.2f %s", (model.income - model.expense), "EUR"));
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, AccountActivity.class);
            intent.putExtra("ID", ID);
            context.startActivity(intent);
        });
    }

    @Override
    public AccountHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_balance, parent, false);
        return new AccountHolder(v);
    }

    class AccountHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView amount;

        public AccountHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
        }
    }

    @Override
    public void onDataChanged()
    {
        income = 0f;
        expense = 0f;

        for (int i = 0; i < getSnapshots().size(); i++)
        {
            Account a = getSnapshots().get(i);
            income += a.income;
            expense += a.expense;
        }

        listener.onAggregationComplete(income - expense);
    }

    public interface AccountAdapterListener
    {
        void onAggregationComplete(float balanceTotal);
    }
}
