/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Fragments.ListContactsFragment;
import com.pagiringi.budgeteer.Fragments.ListContactsFragment.OnSelectListener;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder>
        implements Filterable
{
    private final OnSelectListener listener;
    private List<Contact> contacts;
    private List<Contact> filteredContacts = new ArrayList<>();

    public ContactListAdapter(List<Contact> contacts,
            ListContactsFragment.OnSelectListener listener)
    {
        this.contacts = contacts;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_contact,
                        parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int i)
    {
        Contact model = filteredContacts.get(i);
        if (model.contactPhoto != null)
        {
            Glide.with(holder.avatar.getContext()).load(model.contactPhoto).into(holder.avatar);
        }
        else
        {
            Glide.with(holder.avatar.getContext()).load(R.drawable.ic_account_circle_secondary_24dp).into(
                    holder.avatar);
        }
        holder.name.setText(model.contactName);
        holder.email.setText(model.customEmail);
        holder.view.setOnClickListener(v ->
        {
            if (listener != null)
            {
                listener.onSelect(model);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return filteredContacts.size();
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();

                if (constraint != null && constraint.length() > 0)
                {
                    List<Contact> filterList = new ArrayList<>();
                    for (int i = 0; i < contacts.size(); i++)
                    {
                        Log.d("filter", contacts.get(i).toString().toUpperCase());

                        if ((contacts.get(i)
                                .toString()
                                .toUpperCase()).contains(constraint.toString().toUpperCase()))
                        {
                            filterList.add(contacts.get(i));
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                }
                else
                {
                    results.count = contacts.size();
                    results.values = contacts;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                filteredContacts = (List<Contact>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final View view;
        public final ImageView avatar;
        public final TextView name;
        public final TextView email;

        public ViewHolder(View view)
        {
            super(view);
            this.view = view;
            avatar = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            email = view.findViewById(R.id.custom);
        }
    }
}
