/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.ContactActivity;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.R;

import androidx.recyclerview.widget.RecyclerView;

public class ContactAdapter extends FirestoreRecyclerAdapter<Contact,ContactAdapter.ContactHolder>
{
    public ContactAdapter(FirestoreRecyclerOptions<Contact> options)
    {
        super(options);
    }

    @Override
    protected void onBindViewHolder(ContactHolder holder, int position, Contact model)
    {
        if (model.contactPhoto != null)
        {
            Glide.with(holder.avatar.getContext())
                    .load(model.contactPhoto)
                    .into(holder.avatar);
        }
        else
        {
            Glide.with(holder.avatar.getContext())
                    .load(R.drawable.ic_account_circle_secondary_24dp)
                    .into(holder.avatar);
        }
        holder.name.setText(model.contactName);
        holder.email.setText(model.customEmail);
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, ContactActivity.class);
            intent.putExtra("contactID", model.contactID);
            context.startActivity(intent);
        });
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_contact, parent, false);
        return new ContactHolder(v);
    }

    class ContactHolder extends RecyclerView.ViewHolder
    {
        ImageView avatar;
        TextView name;
        TextView email;

        public ContactHolder(View itemView)
        {
            super(itemView);
            avatar = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.custom);
        }
    }
}
