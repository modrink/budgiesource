/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.ContactActivity;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.R;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Locale;

public class DebtAdapter extends FirestoreRecyclerAdapter<Contact,DebtAdapter.DebtHolder>
{
    private DebtAdapter.DebtAdapterListener listener;
    public float income;
    public float expense;

    public DebtAdapter(FirestoreRecyclerOptions<Contact> options)
    {
        super(options);
        this.listener = null;
    }

    public void setListener(DebtAdapter.DebtAdapterListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(DebtHolder holder, int position, Contact model)
    {
        holder.name.setText(model.contactName);
        holder.email.setText(model.customEmail);
        holder.amount.setText(String.format(Locale.getDefault(), "%.2f %s", (model.in - model.out), "EUR"));
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, ContactActivity.class);
            intent.putExtra("contactID", model.contactID);
            context.startActivity(intent);
        });
    }

    @Override
    public DebtHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_debt, parent, false);
        return new DebtHolder(v);
    }

    class DebtHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView email;
        TextView amount;

        public DebtHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.custom);
            amount = itemView.findViewById(R.id.amount);
        }
    }

    @Override
    public void onDataChanged()
    {
        income = 0f;
        expense = 0f;

        for (int i = 0; i < getSnapshots().size(); i++)
        {
            Contact c = getSnapshots().get(i);
            income += c.in;
            expense += c.out;
        }

        listener.onAggregationComplete(income - expense);
    }

    public interface DebtAdapterListener
    {
        void onAggregationComplete(float balanceTotal);
    }
}
