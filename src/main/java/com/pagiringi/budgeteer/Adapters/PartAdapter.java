/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity;
import com.pagiringi.budgeteer.Firestore.Part;
import com.pagiringi.budgeteer.R;

import java.text.DateFormat;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

public class PartAdapter extends FirestoreRecyclerAdapter<Part,PartAdapter.PartHolder>
{
    private SplitAdapterListener listener;
    public float income;
    public float expense;

    public PartAdapter(FirestoreRecyclerOptions<Part> options)
    {
        super(options);
        this.listener = null;
    }

    public void setListener(SplitAdapterListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(PartHolder holder, int position, Part model)
    {
        holder.date.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
                .format(model.date.toDate()));
        holder.name.setText(model.title);
        holder.amount.setText(String.format(Locale.getDefault(), "%.2f %s", model.sum, "EUR"));
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, PaymentEditActivity.class);
            intent.putExtra("splitID", model.splitID);
            context.startActivity(intent);
        });
    }

    @Override
    public PartHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_payment,
                parent,
                false);
        return new PartHolder(v);
    }

    @Override
    public void onDataChanged()
    {
        income = 0f;
        expense = 0f;

        for (int i = 0; i < getSnapshots().size(); i++)
        {
            Part p = getSnapshots().get(i);

            if (p.sum < 0)
            {
                expense += p.sum;
            }
            else
            {
                income += p.sum;
            }
        }

        listener.onAggregationComplete(income, expense);
    }

    class PartHolder extends RecyclerView.ViewHolder
    {
        TextView date;
        TextView name;
        TextView amount;

        public PartHolder(View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
        }
    }

    public interface SplitAdapterListener
    {
        void onAggregationComplete(float income, float expense);
    }
}
