/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedAccountDTO;
import com.pagiringi.budgeteer.Fragments.ListSelectedAccountsFragment.SelectedAccountsListener;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;

public class SelectedAccountListAdapter extends RecyclerView.Adapter<SelectedAccountListAdapter.ViewHolder>
{
    private Context context;
    private final ArrayList<SelectedAccountDTO> accounts;
    private final SelectedAccountsListener listener;

    public SelectedAccountListAdapter(ArrayList<SelectedAccountDTO> contacts, SelectedAccountsListener listener,
            Context context)
    {
        this.accounts = contacts;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_selected_account, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int i)
    {
        SelectedAccountDTO model = accounts.get(i);
        Glide.with(holder.image.getContext()).load(R.drawable.ic_balance_secondary_24dp).into(holder.image);
        holder.name.setText(model.account.name);
        float balance = model.account.income - model.account.expense;
        holder.balance.setText(String.format(context.getString(R.string.balance), String.valueOf(balance)));
        holder.sum.setText(model.sum);
        holder.button.setOnClickListener(v ->
        {
            if (listener != null)
            {
                listener.onRemove(model);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return accounts.size();
    }

    public ArrayList<SelectedAccountDTO> getItems()
    {
        return accounts;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final ImageView image;
        public final TextView name;
        public final TextView balance;
        public final EditText sum;
        public final ImageButton button;

        public ViewHolder(View view)
        {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            balance = view.findViewById(R.id.custom);
            sum = view.findViewById(R.id.sum);
            button = view.findViewById(R.id.remove);

            sum.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    accounts.get(getAdapterPosition()).sum = s.toString();
                }
            });
        }
    }
}