/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import java.text.DateFormat;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

public class IncomingAdapter extends FirestoreRecyclerAdapter<Split,IncomingAdapter.SplitHolder>
{
    public IncomingAdapter(FirestoreRecyclerOptions<Split> options)
    {
        super(options);
    }

    @Override
    protected void onBindViewHolder(SplitHolder holder, int position, Split model)
    {
        holder.date.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
                .format(model.customDate.toDate()));
        holder.name.setText(model.customTitle);
        // Is payer
        if (model.payerID.equals(User.getCurrentUid()))
        {
            Float sum = Math.abs(model.total);
            holder.amount.setText(String.format("%.2f %s", -sum, "EUR"));
        }
        // Is receiver
        else
        {
            Float sum = Math.abs(model.total);
            holder.amount.setText(String.format("%.2f %s", sum, "EUR"));
        }
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, PaymentEditActivity.class);
            intent.putExtra("paymentID", model.paymentID);
            context.startActivity(intent);
        });
    }

    @Override
    public SplitHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_payment,
                parent,
                false);
        return new SplitHolder(v);
    }

    class SplitHolder extends RecyclerView.ViewHolder
    {
        TextView date;
        TextView name;
        TextView amount;

        public SplitHolder(View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
        }
    }
}
