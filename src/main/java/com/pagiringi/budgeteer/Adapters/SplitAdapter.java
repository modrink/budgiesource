/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.R;

import java.text.DateFormat;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

public class SplitAdapter extends FirestoreRecyclerAdapter<Split,SplitAdapter.SplitHolder>
{
    private SplitAdapterListener listener;
    public float income;
    public float expense;

    public SplitAdapter(FirestoreRecyclerOptions<Split> options)
    {
        super(options);
        this.listener = null;
    }

    public void setListener(SplitAdapterListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(SplitHolder holder, int position, Split model)
    {
        String ID = getSnapshots().getSnapshot(position).getId();

        holder.date.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
                .format(model.customDate.toDate()));
        holder.name.setText(model.customTitle);
        float temp = 0;
        if (model.share < 0)
        {
            temp += model.sum;
        }
        else
        {
            temp += model.sum;
        }
        holder.amount.setText(String.format("%.2f %s", temp, "EUR"));
        holder.itemView.setOnClickListener(v ->
        {
            Context context = v.getContext();
            Intent intent = new Intent(context, PaymentEditActivity.class);
            intent.putExtra("splitID", ID);
            context.startActivity(intent);
        });
    }

    @Override
    public SplitHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_payment,
                parent,
                false);
        return new SplitHolder(v);
    }

    @Override
    public void onDataChanged()
    {
        income = 0f;
        expense = 0f;

        for (int i = 0; i < getSnapshots().size(); i++)
        {
            Split s = getSnapshots().get(i);

            if (s.share < 0)
            {
                expense += s.sum;
            }
            else
            {
                income += s.sum;
            }
        }

        listener.onAggregationComplete(income, expense);
    }

    class SplitHolder extends RecyclerView.ViewHolder
    {
        TextView date;
        TextView name;
        TextView amount;

        public SplitHolder(View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
        }
    }

    public interface SplitAdapterListener
    {
        void onAggregationComplete(float income, float expense);
    }
}
