/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Fragments.ListAccountsFragment.OnSelectListener;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;
import java.util.List;

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.ViewHolder> implements Filterable
{
    private Context context;
    private final OnSelectListener listener;
    private List<Account> accounts;
    private List<Account> filteredAccounts = new ArrayList<>();

    public AccountListAdapter(List<Account> accounts, OnSelectListener listener, Context context)
    {
        this.accounts = accounts;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_contact, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int i)
    {
        Account model = filteredAccounts.get(i);
        Glide.with(holder.avatar.getContext()).load(R.drawable.ic_balance_secondary_24dp).into(holder.avatar);
        holder.name.setText(model.name);
        float balance = model.income - model.expense;
        holder.balance.setText(String.format(context.getString(R.string.balance), String.valueOf(balance)));
        holder.view.setOnClickListener(v ->
        {
            if (listener != null)
            {
                listener.onSelect(model);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return filteredAccounts.size();
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();

                if (constraint != null && constraint.length() > 0)
                {
                    List<Account> filterList = new ArrayList<>();
                    for (int i = 0; i < accounts.size(); i++)
                    {
                        if ((accounts.get(i).toString().toUpperCase()).contains(constraint.toString().toUpperCase()))
                        {
                            filterList.add(accounts.get(i));
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                }
                else
                {
                    results.count = accounts.size();
                    results.values = accounts;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                filteredAccounts = (List<Account>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final View view;
        public final ImageView avatar;
        public final TextView name;
        public final TextView balance;

        public ViewHolder(View view)
        {
            super(view);
            this.view = view;
            avatar = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            balance = view.findViewById(R.id.custom);
        }
    }
}
