/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.google.android.material.snackbar.Snackbar;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedAccountDTO;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.Fragments.ListAccountsFragment;
import com.pagiringi.budgeteer.Fragments.ListSelectedAccountsFragment;
import com.pagiringi.budgeteer.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AccountSplitActivity extends PinCompatActivity
        implements ListAccountsFragment.OnSelectListener, ListSelectedAccountsFragment.SelectedAccountsListener
{
    private ListAccountsFragment searchFragment = new ListAccountsFragment();
    private ListSelectedAccountsFragment selectedFragment = new ListSelectedAccountsFragment();
    private FragmentManager fragMan = getSupportFragmentManager();
    private Fragment curFrag;
    private androidx.appcompat.widget.SearchView sv;
    // Get users from DB
    private List<Account> allAccounts = new ArrayList<>();
    private List<Account> remainingAccounts = new ArrayList<>(allAccounts);
    private Set<SelectedAccountDTO> selectedAccounts = new HashSet<>();
    private float totalSum = 0f;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_search);

        // Initialize toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Enable "up" button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize fragment manager
        curFrag = selectedFragment;
        fragMan.beginTransaction().add(R.id.bill_search_fragment, curFrag).commit();

        // Initialize search bar
        sv = findViewById(R.id.bill_search_bar);
        sv.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                refreshFragment(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                refreshFragment(s);
                return false;
            }
        });

        // Get all contacts
        User.getUserAccounts(res ->
        {
            allAccounts.addAll(res);
            updateRemainingUsers();
        });

        // Get selected list from parent activity
        List<SelectedAccountDTO> contactList = getIntent().getParcelableArrayListExtra("accountList");
        selectedAccounts.addAll(contactList);

        String sumString = getIntent().getStringExtra("sum");
        totalSum = (sumString != null && !sumString.isEmpty()) ? Float.valueOf(sumString) : 0f;
        refreshFragment("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Return to previous activity's fragment
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
            case R.id.action_save:
                Intent data = new Intent();
                ArrayList<SelectedAccountDTO> splitList = selectedFragment.getSplitList();
                Float allSum = 0f;
                for (int i = 0; i < splitList.size(); i++)
                {
                    String tempSum = splitList.get(i).sum;
                    if (tempSum.isEmpty())
                    {
                        Snackbar msg =
                                Snackbar.make(findViewById(android.R.id.content), getString(R.string.fields_empty),
                                        Snackbar.LENGTH_LONG);
                        TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                        msgText.setTextColor(Color.WHITE);
                        msg.show();
                        return false;
                    }
                    float tempSumFloat = Float.valueOf(tempSum);
                    float totalSumSignum = Math.signum(totalSum);
                    float tempSumSignum = Math.signum(tempSumFloat);
                    if (totalSumSignum != tempSumSignum)
                    {
                        char totalSumSignumString = totalSumSignum > 0 ? '+' : '-';
                        char tempSumSignumString = tempSumSignum > 0 ? '+' : '-';
                        Snackbar msg = Snackbar.make(findViewById(android.R.id.content),
                                String.format(getString(R.string.incorrect_signum), tempSumSignumString,
                                        totalSumSignumString), Snackbar.LENGTH_LONG);
                        TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                        msgText.setTextColor(Color.WHITE);
                        msg.show();
                        return false;
                    }
                    allSum += tempSumFloat;
                }
                if (totalSum != allSum)
                {
                    @SuppressLint("StringFormatMatches")
                    Snackbar msg = Snackbar.make(findViewById(android.R.id.content),
                            String.format(getString(R.string.column_sum_big), allSum, totalSum), Snackbar.LENGTH_LONG);
                    TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                    msgText.setTextColor(Color.WHITE);
                    msg.show();
                    return false;
                }
                data.putParcelableArrayListExtra("accountList", splitList);
                setResult(RESULT_OK, data);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public void onSelect(Account account)
    {
        selectedAccounts.add(new SelectedAccountDTO(account, "0"));
        remainingAccounts.remove(account);
        sv.setQuery("", false);
    }

    @Override
    public void onRemove(SelectedAccountDTO account)
    {
        selectedAccounts.remove(account);
        remainingAccounts.add(account.account);
        refreshFragment("");
    }

    private void updateRemainingUsers()
    {
        remainingAccounts = new ArrayList<>(allAccounts);

        for (SelectedAccountDTO selected : selectedAccounts)
        {
            for (Iterator<Account> iterator = remainingAccounts.iterator(); iterator.hasNext(); )
            {
                if (iterator.next().uid.equals(selected.account.uid))
                {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    private void refreshFragment(String filter)
    {
        if (filter.length() == 0)
        {
            selectedFragment.setContacts(new ArrayList<>(selectedAccounts));
            curFrag = selectedFragment;
        }
        else
        {
            searchFragment.setContacts(remainingAccounts);
            searchFragment.setFilter(filter);
            curFrag = searchFragment;
        }

        fragMan.beginTransaction().replace(R.id.bill_search_fragment, curFrag).addToBackStack(null).commit();
    }
}