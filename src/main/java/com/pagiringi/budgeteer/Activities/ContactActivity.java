/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.Query;
import com.pagiringi.budgeteer.Adapters.IncomingAdapter;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ContactActivity extends PinCompatActivity
{
    private Contact contact = new Contact();
    private User contactUser = new User();
    private boolean editing = false;
    private RecyclerView.LayoutManager incomingLM;
    private RecyclerView.LayoutManager outgoingLM;
    private RecyclerView incomingRV;
    private RecyclerView outgoingRV;
    private IncomingAdapter incomingAdapter;
    private IncomingAdapter outgoingAdapter;
    private TextView emailText;
    private TextView phoneText;
    private TextView notesText;
    private EditText emailEdit;
    private EditText phoneEdit;
    private EditText notesEdit;

    @Override
    public void onStart()
    {
        super.onStart();
        incomingAdapter.startListening();
        outgoingAdapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        incomingAdapter.stopListening();
        outgoingAdapter.stopListening();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        ImageView avatar = findViewById(R.id.image);
        emailText = findViewById(R.id.user_email);
        phoneText = findViewById(R.id.user_phone);
        notesText = findViewById(R.id.user_notes);
        emailEdit = findViewById(R.id.user_email_edit);
        phoneEdit = findViewById(R.id.user_phone_edit);
        notesEdit = findViewById(R.id.user_notes_edit);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v ->
        {
            editing = !editing;

            if (editing)
            {
                fab.setImageDrawable(ContextCompat.getDrawable(this,
                        R.drawable.ic_check_white_24dp));
                emailText.setVisibility(View.GONE);
                phoneText.setVisibility(View.GONE);
                notesText.setVisibility(View.GONE);
                emailEdit.setVisibility(View.VISIBLE);
                phoneEdit.setVisibility(View.VISIBLE);
                notesEdit.setVisibility(View.VISIBLE);
            }
            else
            {
                fab.setImageDrawable(ContextCompat.getDrawable(this,
                        R.drawable.ic_edit_white_24dp));
                emailText.setVisibility(View.VISIBLE);
                phoneText.setVisibility(View.VISIBLE);
                notesText.setVisibility(View.VISIBLE);
                emailEdit.setVisibility(View.GONE);
                phoneEdit.setVisibility(View.GONE);
                notesEdit.setVisibility(View.GONE);
                //save data
                String email = emailEdit.getText().toString();
                String phone = phoneEdit.getText().toString();
                String notes = notesEdit.getText().toString();
                contact.customEmail = email.isEmpty() ? null : email;
                contact.customPhone = phone.isEmpty() ? null : phone;
                contact.customDescription = notes.isEmpty() ? null : notes;
                contact.save(null);
                updateFields();
            }
        });

        contact.userID = User.getCurrentUid();
        contact.contactID = getIntent().getStringExtra("contactID");
        contact.load(res ->
        {
            contactUser = new User();
            contactUser.uid = contact.contactID;
            contactUser.load(res2 ->
            {
                // Photo
                if (this.contact.contactPhoto != null && !this.contact.contactPhoto.isEmpty())
                {
                    Glide.with(this).load(this.contact.contactPhoto).into(avatar);
                }
                else
                {
                    Glide.with(this).load(R.drawable.ic_account_circle_secondary_24dp).into(avatar);
                }

                // Name
                if (this.contact.contactName != null && !this.contact.contactName.isEmpty())
                {
                    collapsingToolbar.setTitle(this.contact.contactName);
                }

                updateFields();
            });
        });

        //Incoming split
        Query incomingQuery = Split.getCollection().whereEqualTo("payerID", contact.contactID);
        incomingQuery = incomingQuery.whereEqualTo("receiverID", User.getCurrentUid());
        FirestoreRecyclerOptions<Split> incomingOptions =
                new FirestoreRecyclerOptions.Builder<Split>().setQuery(incomingQuery, Split.class)
                        .build();
        incomingRV = findViewById(R.id.incoming_debts);
        incomingLM = new LinearLayoutManager(this);
        incomingRV.setLayoutManager(incomingLM);
        incomingAdapter = new IncomingAdapter(incomingOptions);
        incomingRV.setAdapter(incomingAdapter);

        //Outgoing split
        Query outgoingQuery = Split.getCollection().whereEqualTo("payerID", User.getCurrentUid());
        outgoingQuery = outgoingQuery.whereEqualTo("receiverID", contact.contactID);
        FirestoreRecyclerOptions<Split> outgoingOptions =
                new FirestoreRecyclerOptions.Builder<Split>().setQuery(outgoingQuery, Split.class)
                        .build();
        outgoingRV = findViewById(R.id.outgoing_debts);
        outgoingLM = new LinearLayoutManager(this);
        outgoingRV.setLayoutManager(outgoingLM);
        outgoingAdapter = new IncomingAdapter(outgoingOptions);
        outgoingRV.setAdapter(outgoingAdapter);
    }

    private void updateFields()
    {
        // Email
        if (contact.customEmail != null && !contact.customEmail.isEmpty())
        {
            emailText.setText(contact.customEmail);
            emailEdit.setText(contact.customEmail);
        }
        else if (contactUser.email != null && !contactUser.email.isEmpty())
        {
            emailText.setText(contactUser.email);
        }
        else
        {
            emailText.setText(getString(R.string.no_email));
        }

        // Phone
        if (contact.customPhone != null && !contact.customPhone.isEmpty())
        {
            phoneText.setText(contact.customPhone);
            phoneEdit.setText(contact.customPhone);
        }
        else if (contactUser.phone != null && !contactUser.phone.isEmpty())
        {
            phoneText.setText(contactUser.phone);
        }
        else
        {
            phoneText.setText(getString(R.string.no_phone));
        }

        // Description
        if (contact.customDescription != null && !contact.customDescription.isEmpty())
        {
            notesText.setText(contact.customDescription);
            notesEdit.setText(contact.customDescription);
        }
        else
        {
            notesText.setText(getString(R.string.no_description));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                assert intent != null;
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                onBackPressed();
                return true;
            case R.id.action_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.remove_contact));
                builder.setMessage(getString(R.string.remove_contact_confirm));
                builder.setPositiveButton(getString(R.string.remove), (dialog, which) ->
                {
                    contact.delete(res2 ->
                    {
                        Contact c2 = new Contact();
                        c2.contactID = contact.userID;
                        c2.userID = contact.contactID;
                        c2.load(res3 ->
                        {
                            res3.delete(res4 ->
                            {
                                finish();
                            });
                        });
                    });

                    dialog.dismiss();
                });
                builder.setNegativeButton(getString(R.string.cancel),
                        (dialog, which) -> dialog.dismiss());
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
