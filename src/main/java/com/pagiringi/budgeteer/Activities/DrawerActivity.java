/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.pagiringi.budgeteer.Fragments.ContactsFragment;
import com.pagiringi.budgeteer.Fragments.PaymentsFragment;
import com.pagiringi.budgeteer.Fragments.OverviewFragment;
import com.pagiringi.budgeteer.Fragments.SettingsFragment;
import com.pagiringi.budgeteer.R;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class DrawerActivity extends PinCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    private NavigationView navView;
    private FragmentManager fragMan;
    private DrawerLayout drawLay;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Initialize layout
        setContentView(R.layout.activity_drawer);

        // Initialize toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Initialize drawer
        drawLay = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawLay,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawLay.addDrawerListener(toggle);
        toggle.syncState();
        navView = findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);

        // Update drawer header.
        View view = navView.getHeaderView(0);
        TextView name = view.findViewById(R.id.name);
        TextView email = view.findViewById(R.id.custom);
        ImageView avatar = view.findViewById(R.id.image);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null)
        {
            String nameStr = currentUser.getDisplayName();
            String emailStr = currentUser.getEmail();
            Uri photoUrl = currentUser.getPhotoUrl();

            name.setText(nameStr);
            email.setText(emailStr);
            if (photoUrl != null)
            {
                Glide.with(this).load(photoUrl).into(avatar);
            }
            else
            {
                Glide.with(this).load(R.drawable.ic_account_circle_white_24dp).into(avatar);
            }
        }

        // Initialize fragment manager
        fragMan = getSupportFragmentManager();
        Fragment frag = new OverviewFragment();
        fragMan.beginTransaction().replace(R.id.activity_content, frag).commit();
    }

    @Override
    public void onBackPressed()
    {
        if (drawLay.isDrawerOpen(GravityCompat.START))
        {
            drawLay.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        Fragment f = fragMan.findFragmentById(R.id.activity_content);

        if (f instanceof OverviewFragment)
        {
            navView.setCheckedItem(R.id.nav_overview);
        }
        else if (f instanceof PaymentsFragment)
        {
            navView.setCheckedItem(R.id.nav_bills);
        }
        else if (f instanceof ContactsFragment)
        {
            navView.setCheckedItem(R.id.nav_contacts);
        }
        else
        { // SettingsActivity
            navView.setCheckedItem(R.id.nav_settings);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        Fragment frag = null;
        Class fragmentClass = null;

        switch (item.getItemId())
        {
            case R.id.nav_overview:
                fragmentClass = OverviewFragment.class;
                break;
            case R.id.nav_bills:
                fragmentClass = PaymentsFragment.class;
                break;
            case R.id.nav_contacts:
                fragmentClass = ContactsFragment.class;
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                //startActivity(new Intent(this, SettingsActivity.class));
                //drawLay.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_logout:
                signOut();
                return true;
        }

        try
        {
            frag = (Fragment) fragmentClass.newInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Change current fragment
        fragMan.beginTransaction().replace(R.id.activity_content, frag).commit();

        // Change title
        setTitle(item.getTitle());

        // Close drawer
        drawLay.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOut()
    {
        AuthUI.getInstance().signOut(this).addOnCompleteListener(task ->
        {
            LockManager.getInstance().getAppLock().disableAndRemoveConfiguration();
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        });
    }
}
