/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.pagiringi.budgeteer.Activities.PaymentEditActivity.SelectedContactDTO;
import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.Fragments.ListContactsFragment;
import com.pagiringi.budgeteer.Fragments.ListSelectedContactsFragment;
import com.pagiringi.budgeteer.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class ContactSplitActivity extends PinCompatActivity
        implements ListContactsFragment.OnSelectListener, ListSelectedContactsFragment.SelectedContactsListener
{
    private ListContactsFragment searchFragment = new ListContactsFragment();
    private ListSelectedContactsFragment selectedFragment = new ListSelectedContactsFragment();
    private FragmentManager fragMan = getSupportFragmentManager();
    private Fragment curFrag;
    private androidx.appcompat.widget.SearchView sv;
    // Get users from DB
    private List<Contact> allContacts = new ArrayList<>();
    private List<Contact> remainingContacts = new ArrayList<>(allContacts);
    private Set<SelectedContactDTO> selectedContacts = new HashSet<>();
    private float totalSum = 0f;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_search);

        // Initialize toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Enable "up" button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize fragment manager
        curFrag = selectedFragment;
        fragMan.beginTransaction().add(R.id.bill_search_fragment, curFrag).commit();

        // Initialize search bar
        sv = findViewById(R.id.bill_search_bar);
        sv.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                refreshFragment(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                refreshFragment(s);
                return false;
            }
        });

        // Get all contacts
        User.getUserContacts(res ->
        {
            allContacts.addAll(res);
            updateRemainingUsers();
        });

        // Get selected list from parent activity
        List<SelectedContactDTO> contactList = getIntent().getParcelableArrayListExtra("contactList");
        selectedContacts.addAll(contactList);

        String sumString = getIntent().getStringExtra("sum");
        if (selectedContacts.size() == 1)
        {
            SelectedContactDTO split = selectedContacts.iterator().next();
            split.sum = sumString;
            split.share = sumString;
        }

        totalSum = (sumString != null && !sumString.isEmpty()) ? Float.valueOf(sumString) : 0f;
        refreshFragment("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Return to previous activity's fragment
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
            case R.id.action_save:
                Intent data = new Intent();
                ArrayList<SelectedContactDTO> splitList = selectedFragment.getSplitList();

                Float allSum = 0f;
                Float allShare = 0f;
                for (int i = 0; i < splitList.size(); i++)
                {
                    String tempSum = splitList.get(i).sum;
                    String tempShare = splitList.get(i).share;

                    if (tempSum.isEmpty() || tempShare.isEmpty())
                    {
                        Snackbar msg =
                                Snackbar.make(findViewById(android.R.id.content), getString(R.string.fields_empty),
                                        Snackbar.LENGTH_LONG);
                        TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                        msgText.setTextColor(Color.WHITE);
                        msg.show();
                        return false;
                    }

                    allSum += Float.valueOf(tempSum);
                    allShare += Float.valueOf(tempShare);
                }

                if (!allShare.equals(allSum))
                {
                    Snackbar msg =
                            Snackbar.make(findViewById(android.R.id.content), getString(R.string.column_sum_mismatch),
                                    Snackbar.LENGTH_LONG);
                    TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                    msgText.setTextColor(Color.WHITE);
                    msg.show();
                    return false;
                }

                if (allShare != totalSum || allSum != totalSum)
                {
                    float temp = allSum;
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this).setTitle(getString(R.string.sum_mismatch))
                            .setMessage(getString(R.string.total_sum_mismatch))
                            .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                            .setPositiveButton(getString(R.string.save), (dialog, which) ->
                            {
                                data.putExtra("sum", String.valueOf(temp));
                                data.putParcelableArrayListExtra("contactList", splitList);
                                setResult(RESULT_OK, data);
                                finish();
                            });
                    alertDialog.create().show();
                    return false;
                }

                data.putParcelableArrayListExtra("contactList", splitList);
                setResult(RESULT_OK, data);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public void onSelect(Contact user)
    {
        selectedContacts.add(new SelectedContactDTO(user, "0", "0"));
        remainingContacts.remove(user);
        sv.setQuery("", false);
        //updateRemainingUsers();
    }

    @Override
    public void onRemove(SelectedContactDTO user)
    {
        selectedContacts.remove(user);
        remainingContacts.add(user.contact);
        refreshFragment("");
        //updateRemainingUsers();
    }

    private void updateRemainingUsers()
    {
        remainingContacts = new ArrayList<>(allContacts);

        for (SelectedContactDTO selected : selectedContacts)
        {
            for (Iterator<Contact> iterator = remainingContacts.iterator(); iterator.hasNext(); )
            {
                if (iterator.next().contactID.equals(selected.contact.contactID))
                {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    private void refreshFragment(String filter)
    {
        if (filter.length() == 0)
        {
            selectedFragment.setContacts(new ArrayList<>(selectedContacts));
            curFrag = selectedFragment;
        }
        else
        {
            searchFragment.setContacts(remainingContacts);
            searchFragment.setFilter(filter);
            curFrag = searchFragment;
        }

        fragMan.beginTransaction().replace(R.id.bill_search_fragment, curFrag).addToBackStack(null).commit();
    }
}