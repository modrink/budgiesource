/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.Contact;
import com.pagiringi.budgeteer.Firestore.Part;
import com.pagiringi.budgeteer.Firestore.Payment;
import com.pagiringi.budgeteer.Firestore.Split;
import com.pagiringi.budgeteer.Firestore.Tag;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;

public class PaymentEditActivity extends PinCompatActivity
{
    private Set<String> baseTags = new HashSet<>();
    private boolean doubleBack = false;
    private Calendar date;
    private EditText titleText;
    private EditText descText;
    private EditText sumText;
    private TextView dateText;
    private TextView splitText;
    private TextView accountText;
    private MultiAutoCompleteTextView tagField;
    private Payment payment;
    private Split split = new Split();
    private Account account = new Account();
    private User user = new User();
    private ArrayList<SelectedContactDTO> contactList = new ArrayList<>();
    private ArrayList<SelectedContactDTO> currentContactList = new ArrayList<>();
    private ArrayList<SelectedAccountDTO> accountList = new ArrayList<>();
    private ArrayList<SelectedAccountDTO> currentAccountList = new ArrayList<>();
    private Contact currentUser = new Contact();
    private Timestamp currentDate;
    private String currentTitle;
    private String currentDescription;
    private float currentSum;
    private View splitButton;

    static final int SPLIT_PAYMENT_REQUEST = 1;
    static final int SPLIT_ACCOUNT_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_edit);

        // Init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Enable "up" button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white);

        // Title
        titleText = findViewById(R.id.bill_edit_title_text);

        // Date
        dateText = findViewById(R.id.bill_edit_date_text);
        // default date
        date = new GregorianCalendar();
        dateText.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(date.getTime()));

        // Create listener
        DatePickerDialog.OnDateSetListener datePickerListener = (view, selectedYear, selectedMonth, selectedDay) ->
        {
            date = new GregorianCalendar(selectedYear, selectedMonth, selectedDay);
            dateText.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(date.getTime()));
        };

        // Parse result
        findViewById(R.id.bill_edit_date).setOnClickListener(v ->
        {
            DatePickerDialog dialog =
                    new DatePickerDialog(this, datePickerListener, date.get(Calendar.YEAR), date.get(Calendar.MONTH),
                            date.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });

        // Description
        descText = findViewById(R.id.bill_edit_description_text);

        // Sum
        sumText = findViewById(R.id.bill_edit_sum_text);

        // Account
        accountText = findViewById(R.id.bill_edit_balances_text);

        // Split text
        splitText = findViewById(R.id.split_text);

        // Load data from intent if applicable
        Intent in = getIntent();
        split.uid = in.getStringExtra("splitID");
        split.paymentID = in.getStringExtra("paymentID");

        // User data
        user = new User();
        user.uid = User.getCurrentUid();
        user.load(res ->
        {
            split.userID = user.uid;
            account = new Account();
            account.uid = user.mainAccountID;
            account.load(res2 ->
            {
                currentAccountList.add(new SelectedAccountDTO(res2, "0"));
                updateAccountText(currentAccountList.size());
                loadSplit();
            });

            currentUser.contactID = user.uid;
            currentUser.contactPhoto = user.photo;
            currentUser.contactName = getString(R.string.you);
            currentUser.customEmail = getString(R.string.your_share);
            currentContactList.add(new SelectedContactDTO(currentUser, "0", "0"));
            updateContactsText(currentContactList.size());

            if ((split.uid == null || split.uid.isEmpty()) && (split.paymentID == null || split.paymentID.isEmpty()))
            {
                splitButton.setVisibility(View.VISIBLE);
            }
        });

        // Split account button
        findViewById(R.id.bill_split_accounts).setOnClickListener(v ->
        {
            String sum;
            Intent intent = new Intent(this, AccountSplitActivity.class);
            intent.putParcelableArrayListExtra("accountList", currentAccountList);
            if (split.share != 0f)
            {
                sum = String.valueOf(split.sum);
            }
            else
            {
                sum = sumText.getText().toString();
            }
            intent.putExtra("sum", sum);
            startActivityForResult(intent, SPLIT_ACCOUNT_REQUEST);
        });

        // Split contact button
        splitButton = findViewById(R.id.bill_split_contacts);
        splitButton.setOnClickListener(v ->
        {
            String sum = sumText.getText().toString();
            Intent intent = new Intent(this, ContactSplitActivity.class);
            intent.putParcelableArrayListExtra("contactList", currentContactList);
            intent.putExtra("sum", sum);
            startActivityForResult(intent, SPLIT_PAYMENT_REQUEST);
        });

        // Tags auto complete field
        List<String> suggestions = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, suggestions);
        tagField = findViewById(R.id.bill_edit_tags_text);
        tagField.setAdapter(adapter);
        tagField.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        // Load user suggestions for tags
        Tag t = new Tag();
        t.userID = user.uid;
        t.loadTags(res ->
        {
            for (Tag tag : res)
            {
                if (!suggestions.contains(tag.name))
                {
                    suggestions.add(tag.name);
                }
            }
            adapter.notifyDataSetChanged();
        });
    }

    private void loadSplit()
    {
        // Load split data
        split.load(res ->
        {
            accountText.setText(getString(R.string.ellipsis));
            splitText.setText(getString(R.string.ellipsis));
            payment = new Payment();
            payment.uid = split.paymentID;
            payment.load(res2 ->
            {
                // Load contact splits
                split.loadSplits(splits ->
                {
                    currentContactList = new ArrayList<>();
                    updateContactsText(splits.size());
                    // HashMap
                    HashMap<String,Split> splitMap = new HashMap<>();
                    for (Split split : splits)
                    {
                        splitMap.put(split.userID, split);
                    }
                    for (int i = 0; i < splits.size(); i++)
                    {
                        if (splits.get(i).userID.equals(user.uid) || payment.userID.equals(user.uid))
                        {
                            Contact c = new Contact();
                            c.contactID = splits.get(i).userID;
                            c.userID = user.uid;
                            c.load(res3 ->
                            {
                                if (res3.contactID.equals(user.uid))
                                {
                                    res3 = currentUser;
                                }
                                Split split = splitMap.get(res3.contactID);
                                currentContactList.add(new SelectedContactDTO(res3, String.valueOf(split.sum),
                                        String.valueOf(split.share)));
                            });
                        }
                    }
                });

                if (payment.userID.equals(user.uid))
                {
                    invalidateOptionsMenu();
                    sumText.setText(String.valueOf(payment.sum));
                    sumText.setVisibility(View.VISIBLE);
                    splitButton.setVisibility(View.VISIBLE);
                }
                else
                {
                    sumText.setText(String.valueOf(split.share));
                    sumText.setInputType(InputType.TYPE_NULL);
                    sumText.setTextColor(ContextCompat.getColor(this, R.color.dark_gray));
                }
            });

            // Load custom user payment data
            titleText.setText(split.customTitle);
            dateText.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
                    .format(split.customDate.toDate()));
            descText.setText(split.customDescription);

            // Load split tags
            Tag t = new Tag();
            t.splitID = split.uid;
            t.userID = split.userID;
            t.loadTags(tags ->
            {
                for (Tag tag : tags)
                {
                    tagField.append(tag.name + ", ");
                    baseTags.add(tag.name);
                }
            });

            // Load account splits
            Part p = new Part();
            p.splitID = split.uid;
            p.loadParts(parts ->
            {
                currentAccountList = new ArrayList<>();
                updateAccountText(parts.size());
                // HashMap
                HashMap<String,Part> partMap = new HashMap<>();
                for (Part part : parts)
                {
                    partMap.put(part.accountID, part);
                }
                for (int i = 0; i < parts.size(); i++)
                {
                    Account a = new Account();
                    a.uid = parts.get(i).accountID;
                    a.load(res3 ->
                    {
                        Part part = partMap.get(res3.uid);
                        currentAccountList.add(new SelectedAccountDTO(res3, String.valueOf(part.sum)));
                    });
                }
            });
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case SPLIT_PAYMENT_REQUEST:
                switch (resultCode)
                {
                    case RESULT_OK:
                        contactList = currentContactList;
                        currentContactList = data.getParcelableArrayListExtra("contactList");
                        String sum = data.getStringExtra("sum");
                        if (sum != null && !sum.isEmpty())
                        {
                            sumText.setText(sum);
                        }
                        updateContactsText(currentContactList.size());
                        break;
                    case RESULT_CANCELED:
                        break;
                    default:
                        break;
                }
                break;
            case SPLIT_ACCOUNT_REQUEST:
                switch (resultCode)
                {
                    case RESULT_OK:
                        accountList = currentAccountList;
                        currentAccountList = data.getParcelableArrayListExtra("accountList");
                        updateAccountText(currentAccountList.size());
                        break;
                    case RESULT_CANCELED:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (payment != null && payment.userID != null && !payment.userID.isEmpty() && user != null &&
                user.uid != null && !user.uid.isEmpty() && payment.userID.equals(user.uid))
        {
            getMenuInflater().inflate(R.menu.menu_delete, menu);
        }

        getMenuInflater().inflate(R.menu.menu_save, menu);

        return true;
    }

    private void updateAccountText(int size)
    {
        String temp = getString(R.string.split_amongst) + " " + size + " ";
        if (size > 1)
        {
            temp += getString(R.string.multiple_accounts);
        }
        else
        {
            temp += getString(R.string.single_account);
        }
        accountText.setText(temp);
    }

    private void updateContactsText(int size)
    {
        String temp = getString(R.string.split_amongst) + " " + size + " ";
        if (size > 1)
        {
            temp += getString(R.string.people);
        }
        else
        {
            temp += getString(R.string.person);
        }
        splitText.setText(temp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Return to previous activity's fragment
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
            case R.id.action_delete:
                // Remove contact splits
                AlertDialog.Builder alertDialog =
                        new AlertDialog.Builder(this).setTitle(getString(R.string.remove_payment))
                                .setMessage(getString(R.string.confirm_payment_remove))
                                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                                .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                                {
                                    for (SelectedContactDTO before : currentContactList)
                                    {
                                        removeContactSplit(before);
                                    }
                                    payment.delete(null);
                                    finish();
                                });
                alertDialog.create().show();
                break;
            case R.id.action_save:
                // Insert new payment
                currentTitle = titleText.getText().toString();
                if (currentTitle.isEmpty())
                {
                    titleText.setError(getString(R.string.field_required));
                    return false;
                }
                currentDate = new Timestamp(date.getTime());
                currentDescription = descText.getText().toString();
                try
                {
                    currentSum = Float.parseFloat(sumText.getText().toString());
                }
                catch (NumberFormatException e)
                {
                    sumText.setError(getString(R.string.field_required));
                    return false;
                }
                if (currentSum == 0)
                {
                    sumText.setError(getString(R.string.sum_zero));
                    return false;
                }
                // No contact split specified
                if (contactList.size() == 0)
                {
                    contactList = currentContactList;
                }
                // No account split specified
                if (accountList.size() == 0)
                {
                    accountList = currentAccountList;
                }
                // Only one contact
                if (currentContactList.size() == 1 &&
                        (currentContactList.get(0).share == null || currentContactList.get(0).share.isEmpty() ||
                                currentContactList.get(0).share.equals("0") ||
                                currentContactList.get(0).share.equals("0.0") ||
                                currentContactList.get(0).sum == null || currentContactList.get(0).sum.isEmpty() ||
                                currentContactList.get(0).sum.equals("0") ||
                                currentContactList.get(0).sum.equals("0.0")))
                {
                    currentContactList.get(0).share = String.valueOf(currentSum);
                    currentContactList.get(0).sum = String.valueOf(currentSum);
                }
                // Only one account
                if (currentAccountList.size() == 1)
                {
                    for (SelectedContactDTO contactDTO : currentContactList)
                    {
                        if (contactDTO.contact.contactID.equals(user.uid))
                        {
                            currentAccountList.get(0).sum = String.valueOf(contactDTO.sum);
                            break;
                        }
                    }
                }


                // Save all data
                // Existing payment
                if (payment != null)
                {
                    // Owner of group payment
                    if (payment.userID.equals(user.uid))
                    {
                        payment.title = currentTitle;
                        payment.date = currentDate;
                        payment.description = currentDescription;
                        payment.sum = currentSum;
                        payment.save(null);

                        // Add or update contact splits
                        for (SelectedContactDTO current : currentContactList)
                        {
                            boolean add = true;
                            for (SelectedContactDTO before : contactList)
                            {
                                // Existing contact
                                if (current.contact.contactID.equals(before.contact.contactID))
                                {
                                    add = false;
                                    updateContactSplit(current);
                                    break;
                                }
                            }
                            if (add)
                            {
                                addContactSplit(current);
                            }
                        }

                        // Remove contact splits
                        for (SelectedContactDTO before : contactList)
                        {
                            boolean remove = true;
                            for (SelectedContactDTO current : currentContactList)
                            {
                                if (before.contact.contactID.equals(current.contact.contactID))
                                {
                                    remove = false;
                                    break;
                                }
                            }
                            if (remove)
                            {
                                removeContactSplit(before);
                            }
                        }
                    }
                    else
                    {
                        updateContactSplit(currentContactList.get(0));
                    }
                }
                // New payment
                else
                {
                    payment = new Payment();
                    payment.title = currentTitle;
                    payment.date = currentDate;
                    payment.description = currentDescription;
                    payment.sum = currentSum;
                    payment.userID = user.uid;
                    payment.save(res ->
                    {
                        for (SelectedContactDTO current : currentContactList)
                        {
                            addContactSplit(current);
                        }
                    });
                }

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addContactSplit(SelectedContactDTO data)
    {
        Split s = new Split();
        s.customDate = currentDate;
        s.customDescription = currentDescription;
        s.customTitle = currentTitle;
        s.paymentID = payment.uid;
        Float share = Float.valueOf(data.share);
        s.share = share;
        Float sum = Float.valueOf(data.sum);
        s.sum = sum;
        s.total = share - sum;
        if (s.total > 0)
        {
            s.payerID = user.uid;
            s.receiverID = data.contact.contactID;

            if (!data.contact.contactID.equals(user.uid))
            {
                data.contact.out += Math.abs(s.total);
                data.contact.save(null);

                Contact c = new Contact();
                c.contactID = user.uid;
                c.userID = data.contact.contactID;
                c.load(res ->
                {
                    res.in += Math.abs(s.total);
                    res.save(null);
                });
            }
        }
        else if (s.total < 0)
        {
            s.payerID = data.contact.contactID;
            s.receiverID = user.uid;

            if (!data.contact.contactID.equals(user.uid))
            {
                data.contact.in += Math.abs(s.total);
                data.contact.save(null);

                Contact c = new Contact();
                c.contactID = user.uid;
                c.userID = data.contact.contactID;
                c.load(res ->
                {
                    res.out += Math.abs(s.total);
                    res.save(null);
                });
            }
        }
        s.userID = data.contact.contactID;
        s.save(res ->
        {
            // Set tags
            if (s.userID.equals(user.uid))
            {
                setTags(res);
                // Add or update account splits
                for (SelectedAccountDTO current : currentAccountList)
                {
                    addAccountSplit(current, res);
                }
            }
            else
            {
                User u = new User();
                u.uid = data.contact.contactID;
                u.load(res2 ->
                {
                    // New part
                    Part p = new Part();
                    p.splitID = res.uid;
                    p.date = s.customDate;
                    p.accountID = res2.mainAccountID;
                    p.sum = s.sum;
                    p.title = s.customTitle;
                    p.save(null);
                    // Update account
                    Account a = new Account();
                    a.uid = res2.mainAccountID;
                    a.load(res3 ->
                    {
                        if (sum > 0)
                        {
                            a.income += s.sum;
                        }
                        else if (sum < 0)
                        {
                            a.expense -= s.sum;
                        }
                        a.save(null);
                    });
                });
            }
        });
    }

    private void removeContactSplit(SelectedContactDTO data)
    {
        Split s = new Split();
        s.paymentID = payment.uid;
        s.userID = data.contact.contactID;
        s.load(split ->
        {
            // Remove parts
            Part p = new Part();
            p.splitID = split.uid;
            p.loadParts(parts ->
            {
                for (Part part : parts)
                {
                    Account a = new Account();
                    a.uid = part.accountID;
                    a.load(accountModel ->
                    {
                        SelectedAccountDTO selectedAccountDTO = new SelectedAccountDTO();
                        selectedAccountDTO.account = accountModel;
                        selectedAccountDTO.sum = String.valueOf(part.sum);
                        removeAccountSplit(selectedAccountDTO, split);
                    });
                }
            });
            // Remove tags
            Tag t = new Tag();
            t.splitID = split.uid;
            t.userID = split.userID;
            t.loadTags(tags ->
            {
                for (Tag tag : tags)
                {
                    tag.delete(null);
                }
            });
            // Update contacts
            Contact c = new Contact();
            c.contactID = user.uid;
            c.userID = data.contact.contactID;
            c.load(res2 ->
            {
                if (split.total > 0)
                {
                    data.contact.out -= Math.abs(split.total);
                    res2.in -= Math.abs(split.total);
                }
                else if (split.total < 0)
                {
                    data.contact.in -= Math.abs(split.total);
                    res2.out -= Math.abs(split.total);
                }
                if (!data.contact.contactID.equals(user.uid))
                {
                    data.contact.save(null);
                    res2.save(null);
                }
            });
            // Remove split
            split.delete(null);
        });
    }

    private void updateContactSplit(SelectedContactDTO data)
    {
        Split s = new Split();
        s.paymentID = payment.uid;
        s.userID = data.contact.contactID;
        s.load(res ->
        {
            // Owner of split
            if (s.userID.equals(user.uid))
            {
                s.customTitle = currentTitle;
                s.customDate = currentDate;
                s.customDescription = currentDescription;
                setTags(res);

                // Add or update account splits
                for (SelectedAccountDTO current : currentAccountList)
                {
                    boolean add = true;
                    for (SelectedAccountDTO before : accountList)
                    {
                        // Existing contact
                        if (current.account.uid.equals(before.account.uid))
                        {
                            add = false;
                            updateAccountSplit(current, res);
                            break;
                        }
                    }
                    if (add)
                    {
                        addAccountSplit(current, res);
                    }
                }

                // Remove account splits
                for (SelectedAccountDTO before : accountList)
                {
                    boolean remove = true;
                    for (SelectedAccountDTO current : currentAccountList)
                    {
                        if (before.account.uid.equals(current.account.uid))
                        {
                            remove = false;
                            break;
                        }
                    }
                    if (remove)
                    {
                        removeAccountSplit(before, res);
                    }
                }
            }
            else
            {
                Float sumBefore = s.sum;
                Float newSum = Float.valueOf(data.sum);

                User u = new User();
                u.uid = s.userID;
                u.load(res2 ->
                {
                    // Update account
                    Account a = new Account();
                    a.uid = res2.mainAccountID;
                    a.load(res3 ->
                    {
                        if (newSum > 0)
                        {
                            a.income -= sumBefore;
                            a.income += newSum;
                        }
                        else if (newSum < 0)
                        {
                            a.expense += sumBefore;
                            a.expense -= newSum;
                        }
                        a.save(null);
                    });
                    // Update part
                    Part p = new Part();
                    p.splitID = res.uid;
                    p.accountID = res2.mainAccountID;
                    p.load(res3 ->
                    {
                        p.date = s.customDate;
                        p.sum = newSum;
                        p.title = s.customTitle;
                        p.save(null);
                    });
                });

                s.share = Float.valueOf(data.share);
                s.sum = newSum;
                float totalBefore = s.total;
                s.total = s.share - s.sum;

                Contact c = new Contact();
                c.contactID = user.uid;
                c.userID = data.contact.contactID;
                c.load(res2 ->
                {
                    if (totalBefore > 0)
                    {
                        data.contact.out -= Math.abs(totalBefore);
                        res2.in -= Math.abs(totalBefore);
                    }
                    else if (totalBefore < 0)
                    {
                        data.contact.in -= Math.abs(totalBefore);
                        res2.out -= Math.abs(totalBefore);
                    }
                    if (s.total > 0)
                    {
                        s.payerID = user.uid;
                        s.receiverID = data.contact.contactID;
                        data.contact.out += Math.abs(s.total);
                        res2.in += Math.abs(s.total);
                    }
                    else if (s.total < 0)
                    {
                        s.payerID = data.contact.contactID;
                        s.receiverID = user.uid;
                        data.contact.in += Math.abs(s.total);
                        res2.out += Math.abs(s.total);
                    }
                    if (!data.contact.contactID.equals(user.uid))
                    {
                        data.contact.save(null);
                        res2.save(null);
                    }
                });
            }

            s.save(null);
        });
    }

    private void addAccountSplit(SelectedAccountDTO data, Split split)
    {
        // New part
        Part p = new Part();
        p.splitID = split.uid;
        p.date = currentDate;
        p.accountID = data.account.uid;
        p.sum = Float.valueOf(data.sum);
        p.title = currentTitle;
        p.save(null);
        // Update account
        Account a = new Account();
        a.uid = data.account.uid;
        a.load(res3 ->
        {
            if (p.sum > 0)
            {
                a.income += p.sum;
            }
            else if (p.sum < 0)
            {
                a.expense -= p.sum;
            }
            a.save(null);
        });
    }

    private void removeAccountSplit(SelectedAccountDTO data, Split split)
    {
        Part p = new Part();
        p.splitID = split.uid;
        p.accountID = data.account.uid;
        p.load(res ->
        {
            // Update account
            Account a = new Account();
            a.uid = data.account.uid;
            a.load(res3 ->
            {
                if (res.sum > 0)
                {
                    a.income -= res.sum;
                }
                else if (res.sum < 0)
                {
                    a.expense += res.sum;
                }
                a.save(null);
            });
            p.delete(null);
        });
    }

    private void updateAccountSplit(SelectedAccountDTO data, Split split)
    {
        Part p = new Part();
        p.splitID = split.uid;
        p.accountID = data.account.uid;
        p.load(res ->
        {
            float sumBefore = p.sum;
            p.sum = Float.valueOf(data.sum);
            p.title = currentTitle;
            p.date = currentDate;
            // Update account
            Account a = new Account();
            a.uid = data.account.uid;
            a.load(res3 ->
            {
                if (p.sum > 0)
                {
                    a.income -= sumBefore;
                    a.income += p.sum;
                }
                else if (p.sum < 0)
                {
                    a.expense += sumBefore;
                    a.expense -= p.sum;
                }
                a.save(null);
            });
            p.save(null);
        });
    }

    private void setTags(Split split)
    {
        Set<String> newTags = splitText(tagField.getText().toString());
        Set<String> toRemove = new HashSet<>(baseTags);
        Set<String> toAdd = new HashSet<>(newTags);
        toRemove.removeAll(newTags);
        toAdd.removeAll(baseTags);

        // Remove tags
        for (String tag : toRemove)
        {
            if (!tag.isEmpty())
            {
                String tagLower = tag.toLowerCase();
                Tag t = new Tag();
                t.name = tagLower;
                t.userID = split.userID;
                t.splitID = split.uid;
                t.load(res -> t.delete(null));
            }
        }

        // Add tags
        for (String tag : toAdd)
        {
            if (!tag.isEmpty())
            {
                String tagLower = tag.toLowerCase();
                Tag t = new Tag();
                t.name = tagLower;
                t.userID = split.userID;
                t.splitID = split.uid;
                t.save(null);
            }
        }

        // Set tags for split
        split.tags = new ArrayList<>(newTags);
        split.save(null);
    }

    private HashSet<String> splitText(String text)
    {
        return new HashSet<>(Arrays.asList(text.split("\\s*,\\s*")));
    }

    @Override
    public void onBackPressed()
    {
        // back button has been pressed twice or there is no data to save
        if (doubleBack /*|| !isDirty()*/)
        {
            // default back navigation
            super.onBackPressed();
        }
        else
        {
            // register back press
            doubleBack = true;
            // show exit alert to user
            Snackbar msg = Snackbar.make(findViewById(android.R.id.content), getString(R.string.discard_confirm),
                    Snackbar.LENGTH_SHORT);
            TextView msgText = msg.getView().findViewById(com.google.android.material.R.id.snackbar_text);
            msgText.setTextColor(Color.WHITE);
            msg.show();
            new android.os.Handler().postDelayed(() ->
            {
                // time sum for second back press
                doubleBack = false;
            }, 2000);
        }
    }

    public static class SelectedContactDTO implements Parcelable
    {
        public Contact contact;
        public String sum;
        public String share;

        public SelectedContactDTO()
        {
        }

        public SelectedContactDTO(Contact contact, String sum, String share)
        {
            this.contact = contact;
            this.sum = sum;
            this.share = share;
        }

        protected SelectedContactDTO(Parcel in)
        {
            contact = in.readParcelable(Contact.class.getClassLoader());
            sum = in.readString();
            share = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeParcelable(contact, flags);
            dest.writeString(sum);
            dest.writeString(share);
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        public static final Creator<SelectedContactDTO> CREATOR = new Creator<SelectedContactDTO>()
        {
            @Override
            public SelectedContactDTO createFromParcel(Parcel in)
            {
                return new SelectedContactDTO(in);
            }

            @Override
            public SelectedContactDTO[] newArray(int size)
            {
                return new SelectedContactDTO[size];
            }
        };
    }

    public static class SelectedAccountDTO implements Parcelable
    {
        public Account account;
        public String sum;

        public SelectedAccountDTO()
        {
        }

        public SelectedAccountDTO(Account account, String sum)
        {
            this.account = account;
            this.sum = sum;
        }

        protected SelectedAccountDTO(Parcel in)
        {
            account = in.readParcelable(Account.class.getClassLoader());
            sum = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeParcelable(account, flags);
            dest.writeString(sum);
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        public static final Creator<SelectedAccountDTO> CREATOR = new Creator<SelectedAccountDTO>()
        {
            @Override
            public SelectedAccountDTO createFromParcel(Parcel in)
            {
                return new SelectedAccountDTO(in);
            }

            @Override
            public SelectedAccountDTO[] newArray(int size)
            {
                return new SelectedAccountDTO[size];
            }
        };
    }
}

