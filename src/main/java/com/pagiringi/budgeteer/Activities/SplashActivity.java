/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.User;
import com.pagiringi.budgeteer.R;

import java.util.Arrays;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity
{
    private static final int RC_SIGN_IN = 101;
    // Splash screen duration
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Enable Lollipin
        LockManager<CustomPinActivity> lockManager = LockManager.getInstance();
        lockManager.enableAppLock(this, CustomPinActivity.class);
        lockManager.getAppLock().setLogoId(R.drawable.ic_lock_custom_24dp);
        //works only between PinCompatActivity activities
        lockManager.getAppLock().setOnlyBackgroundTimeout(true);
        lockManager.getAppLock().addIgnoredActivity(SplashActivity.class);

        createSignInIntent();
    }

    public void createSignInIntent()
    {
        new Handler().postDelayed(() ->
        {
            FirebaseAuth auth = FirebaseAuth.getInstance();
            if (auth.getCurrentUser() != null)
            {
                updateAccount();
                Intent intent = new Intent(this, DrawerActivity.class);
                startActivity(intent);
                finish();
            }
            else
            {
                startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.GoogleBuilder().build(),
                                new AuthUI.IdpConfig.EmailBuilder().build()))
                        .setTosAndPrivacyPolicyUrls(
                                "https://sites.google.com/view/budgie-app/terms-of-service",
                                "https://sites.google.com/view/budgie-app/privacy-policy")
                        .setLogo(R.drawable.budgie)
                        .setTheme(R.style.AppTheme)
                        .build(), RC_SIGN_IN);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void updateAccount()
    {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser == null)
        {
            return;
        }

        //Load user from firebase
        User user = new User();
        user.uid = firebaseUser.getUid();
        user.load(res ->
        {
            user.loadFirebaseData();

            if (user.mainAccountID == null)
            {
                Account account = new Account();
                account.userID = user.uid;
                account.name = "Default";
                account.save(res2 ->
                {
                    user.mainAccountID = res2.uid;
                    user.save(res3 ->
                    {
                    });
                });
            }
            else
            {
                user.save(res2 ->
                {
                });
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign sum flow.
        if (requestCode == RC_SIGN_IN)
        {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed sum
            if (resultCode == RESULT_OK)
            {
                updateAccount();

                Intent intent = new Intent(this, DrawerActivity.class);
                startActivity(intent);
                finish();
            }
            else
            {
                // Sign sum failed
                if (response == null)
                {
                    // User pressed back button
                    showSnackbar(R.string.sign_in_cancelled);
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK)
                {
                    showSnackbar(R.string.no_internet_connection);
                    return;
                }

                showSnackbar(R.string.unknown_error);
                Log.e("FirebaseUIAuth", getString(R.string.sign_in_error), response.getError());
            }
        }
    }

    private void showSnackbar(int strResource)
    {
        Snackbar.make(findViewById(R.id.activity_splash), strResource, Snackbar.LENGTH_SHORT)
                .show();
    }
}

