/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.content.Intent;

import com.firebase.ui.auth.AuthUI;
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity;
import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.google.android.material.snackbar.Snackbar;
import com.pagiringi.budgeteer.R;

import androidx.appcompat.app.AlertDialog;

public class CustomPinActivity extends AppLockActivity
{
    @Override
    public void showForgotDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.pin_forgot));
        builder.setMessage(getString(R.string.forgot_dialog));
        builder.setPositiveButton(getString(R.string.log_out), (dialog, which) ->
        {
            AuthUI.getInstance().signOut(this).addOnCompleteListener(task ->
            {
                LockManager.getInstance().getAppLock().disableAndRemoveConfiguration();
                Intent intent = new Intent(this, SplashActivity.class);
                startActivity(intent);
                finish();
            });
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss());
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onPinFailure(int attempts)
    {
        if (attempts == 3)
        {
            //Snackbar.make(findViewById(R.id.activity_custom_pin), R.string.forgot_pin,Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPinSuccess(int attempts)
    {
        LockManager.getInstance().getAppLock().setTimeout(Long.MAX_VALUE);
    }
}