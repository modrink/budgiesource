/**
 * Copyright (c) 2019 Pagiringi. All rights reserved.
 */
//Modestas Rinkevičius
package com.pagiringi.budgeteer.Activities;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.omadahealth.lollipin.lib.PinCompatActivity;
import com.google.firebase.firestore.Query;
import com.pagiringi.budgeteer.Adapters.PartAdapter;
import com.pagiringi.budgeteer.Firestore.Account;
import com.pagiringi.budgeteer.Firestore.Part;
import com.pagiringi.budgeteer.Money;
import com.pagiringi.budgeteer.R;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AccountActivity extends PinCompatActivity
{
    /**
     * Account fragment layout id.
     */
    final private int layout = R.layout.activity_balance;
    private TextView incomeText;
    private TextView expenditureText;
    private TextView totalText;

    private RecyclerView.LayoutManager partLM;
    private RecyclerView partRV;
    private PartAdapter partAdapter;

    @Override
    public void onStart()
    {
        super.onStart();
        partAdapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        partAdapter.stopListening();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());

        incomeText = findViewById(R.id.balance_income);
        expenditureText = findViewById(R.id.balance_expenditure);
        totalText = findViewById(R.id.balance_total);

        Account a = new Account();
        a.uid = getIntent().getStringExtra("ID");
        a.load(res -> toolbar.setTitle(getString(R.string.account) + " \"" + res.name + "\" "));

        // Account list
        Query accountQuery = Part.getCollection().whereEqualTo("accountID", a.uid);
        FirestoreRecyclerOptions<Part> accountOptions =
                new FirestoreRecyclerOptions.Builder<Part>().setQuery(accountQuery, Part.class)
                        .build();
        partRV = findViewById(R.id.account_list);
        partRV.setHasFixedSize(true);
        partLM = new LinearLayoutManager(this);
        partRV.setLayoutManager(partLM);
        partAdapter = new PartAdapter(accountOptions);
        partAdapter.setListener(this::updateTotals);
        partRV.setAdapter(partAdapter);
    }

    //Osvaldas Čiupkevičius
    private void updateTotals(float income, float expense)
    {
        new Money(income, Money.Currency.EUR).setMoneyView(incomeText, true);
        new Money(expense, Money.Currency.EUR).setMoneyView(expenditureText, true);
        new Money(income + expense, Money.Currency.EUR).setMoneyView(totalText, true);
    }

}
