#Licencija / License

Programinės įrangos autorinės teisės priklauso tik jų autoriams.
Repozitorijoje pateikiama uždaro kodo programinė įranga, jos išeities tekstai.
Suteikiama teisė (leidžiama) demonstruoti, naudotis programine įranga.
Draudžiama kopijuoti, modifikuoti, platinti, suteikti sublicencijas jeigu nėra nurodyta kitaip.

Software copyright is retained by their respective authors.
Repository contains closed source software and source code.
You have the right to perform and display this software.
It is forbidden to copy, modify, distribute or sublicense this software unless stated otherwise.

Naujausia versija pasiekiama:
https://bitbucket.org/modrink/budgiesource/src